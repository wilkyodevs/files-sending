# README #

This README would normally document whatever steps are necessary to get your application up and running.

---

# What is this repository for ? #

Java application to transfer files from a server to clients.

Version: 2.0.0 - RELEASE

---

# How do I get set up? #

## Eclipse

Download and install [Eclipse](https://www.eclipse.org/downloads/) for Java EE developers.
You can put it at your drive root or under a folder like tools or programs.

Go to __Window/Preferences/General/Workspace__ and change the Text file encoding to UTF-8

Go to __Window/Preferences/General/Workspace__ and change the New text file line delimiter to Unix

Install the [Egit plugin for Eclipse](http://download.eclipse.org/egit/updates)

---

# How to run tests

To run the Application, you can just hit run on Eclipse.

---

# Contribution guidelines

Always write a clear commit message to know what it had been done.

Don't forget to git add the new files and git remove the deleted ones.

There is no little commit...

Don't forget to push your changes when finished.

Avoid committing config files (unless you added new configurations).

---

# Who do I talk to ?

Repo owner or admin
