package gui.model;

import application.LanguageHelper;

/**
 * The Model managing the help window.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class OptionsModel {

	/**
	 * Default language.
	 */
	private static final int DEFAULT_LANGUAGE = 1;
	/**
	 * Default file path.
	 */
	private static final String DEFAULT_FILE_PATH = "./";
	/**
	 * Default send and receive port.
	 */
	private static final int DEFAULT_PORT = 4200;
	/**
	 * Default size of the packages to send.
	 */
	private static final int DEFAULT_PACKAGES_SIZE = 1000000;
	/**
	 * Default sending ip.
	 */
	private static final String DEFAULT_SENDER_ADDRESS = "127.0.0.1";

	/**
	 * Language id.
	 */
	private int language;
	/**
	 * File's path.
	 */
	private String filePath;
	/**
	 * The send port.
	 */
	private int sendPort;
	/**
	 * The size of the packages to send.
	 */
	private int packagesSize;
	/**
	 * Server address.
	 */
	private String senderAddress;
	/**
	 * The sender's port.
	 */
	private int senderPort;

	/**
	 * Defines the default Options.
	 */
	public OptionsModel() {
		this.language = DEFAULT_LANGUAGE;
		this.filePath = DEFAULT_FILE_PATH;
		this.sendPort = DEFAULT_PORT;
		this.packagesSize = DEFAULT_PACKAGES_SIZE;
		this.senderAddress = DEFAULT_SENDER_ADDRESS;
		this.senderPort = DEFAULT_PORT;
	}

	/**
	 * Creates a copy of the Options given in parameter.
	 * 
	 * @param options
	 *            OptionsModel to copy
	 */
	public OptionsModel(OptionsModel options) {
		this.language = options.language;
		this.filePath = options.filePath;
		this.sendPort = options.getSendPort();
		this.packagesSize = options.getPackagesSize();
		this.senderAddress = options.senderAddress;
		this.senderPort = options.getSenderPort();
	}

	/**
	 * Returns the language id.
	 * 
	 * @return Integer of the language
	 */
	public int getLanguage() {
		return language;
	}

	/**
	 * Sets the language id.
	 * 
	 * @param language
	 *            Integer of the language
	 */
	public void setLanguage(int language) {
		this.language = language;
		refreshLanguage();
	}

	/**
	 * Resets the language in the LanguageHelper.
	 */
	public void refreshLanguage() {
		LanguageHelper.setLanguage(language);
	}

	/**
	 * Returns the file's path to send.
	 * 
	 * @return String of the absolute path to the file.
	 */
	public String getFilePath() {
		return this.filePath;
	}

	/**
	 * Sets the file's path to send.
	 * 
	 * @param path
	 *            String of the absolute path to the file.
	 */
	public void setFilePath(String path) {
		this.filePath = path;
	}

	/**
	 * Returns the port for the sending.
	 * 
	 * @return Integer of the port to take to send the file.
	 */
	public int getSendPort() {
		return this.sendPort;
	}

	/**
	 * Sets the port for the sending.
	 * 
	 * @param port
	 *            Integer of the port to take to send the file.
	 */
	public void setSendPort(int port) {
		this.sendPort = port;
	}

	/**
	 * Returns the size of the packages to send.
	 * 
	 * @return Integer of the size of the packages to send.
	 */
	public int getPackagesSize() {
		return this.packagesSize;
	}

	/**
	 * Sets the size of the packages to send.
	 * 
	 * @param size
	 *            Integer of the size of the packages to send.
	 */
	public void setPackagesSize(int size) {
		this.packagesSize = size;
	}

	/**
	 * Returns the server address to receive from.
	 * 
	 * @return String of the address
	 */
	public String getSenderAddress() {
		return this.senderAddress;
	}

	/**
	 * Sets the server address to receive from.
	 * 
	 * @param receiveFrom
	 *            String of the address
	 */
	public void setSenderAddress(String receiveFrom) {
		this.senderAddress = receiveFrom;
	}

	/**
	 * Returns the port for the receiving.
	 * 
	 * @return Integer of the port to take to receive the file.
	 */
	public int getSenderPort() {
		return this.senderPort;
	}

	/**
	 * Sets the port for the receiving.
	 * 
	 * @param port
	 *            Integer of the port to take to receive the file.
	 */
	public void setSenderPort(int port) {
		this.senderPort = port;
	}

}
