package gui.model.main;

/**
 * Stores the file's size.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class FileSize {

	/**
	 * Size of the file.
	 */
	private long size = 0;
	/**
	 * Represents the amount of bytes it has.
	 */
	private short degree = 0;

	/**
	 * Initializes the size.
	 */
	public FileSize() {
	}

	/**
	 * Initializes the size.
	 * 
	 * @param size
	 *            Long of the current size downloaded.
	 */
	public FileSize(long size) {
		this.size = size;
	}

	/**
	 * Adds some bytes to the size.
	 * 
	 * @param size
	 *            Long of the size to add.
	 */
	public void add(long size) {
		this.size += size;
		if (this.size >= 1000000000)
			degree = 3;
		else if (this.size >= 1000000)
			degree = 2;
		else if (this.size >= 1000)
			degree = 1;
	}

	/**
	 * Returns the file's size.
	 * 
	 * @return Long of the size of the file.
	 */
	public long getSize() {
		return this.size;
	}

	/**
	 * Sets the file's size.
	 * 
	 * @return Long of the size of the file.
	 */
	public void setSize(long size) {
		this.size = size;
	}

	/**
	 * Returns the output for the file's size.
	 * 
	 * @return String of the size of the file.
	 */
	public String toString() {
		long aux = size;
		String o = "", ko = "", mo = "", go = "";
		o = String.valueOf(aux % 1000);
		if (degree > 0) {
			while (o.length() != 3) {
				o = "0" + o;
			}
			aux = aux / 1000;
			ko = String.valueOf(aux % 1000);
			if (degree > 1) {
				while (ko.length() != 3) {
					ko = "0" + ko;
				}
				aux = aux / 1000;
				mo = String.valueOf(aux % 1000);
				if (degree > 2) {
					while (mo.length() != 3) {
						mo = "0" + mo;
					}
					aux = aux / 1000;
					go = String.valueOf(aux % 1000);
				}
			}
		}
		return (degree < 3 ? "" : go + " Go, ")
				+ (degree < 2 ? "" : mo + " Mo, ")
				+ (degree < 1 ? "" : ko + " Ko, ") + o + " o";
	}
}
