package gui.model.main;

import gui.model.main.event.FileSizeEvent;
import gui.model.main.event.FileSizeListener;
import gui.model.main.exceptions.UnfindableFileSizeException;

import java.util.HashMap;
import java.util.Map;

import javax.swing.event.EventListenerList;

/**
 * Model managing the current size of the downloaded file.
 */
public class FileSizeModel {

	/**
	 * The size of the current file downloaded.
	 */
	Map<User, FileSize> filesSize;
	/**
	 * Listeners list.
	 */
	private EventListenerList filesizeListeners;

	/**
	 * Initializes the FileSizeModel.
	 */
	public FileSizeModel() {
		this.filesizeListeners = new EventListenerList();
		this.filesSize = new HashMap<User, FileSize>();
	}

	/**
	 * Initializes the FileSizeModel with a FileSize.
	 * 
	 * @param user
	 *            An User.
	 * @param size
	 *            The current FileSize.
	 */
	public FileSizeModel(User user, long size) {
		this.filesizeListeners = new EventListenerList();
		this.filesSize = new HashMap<User, FileSize>();
		this.filesSize.put(user, new FileSize(size));
	}

	/**
	 * Gets the file's size.
	 * 
	 * @return FileSize of the file's size.
	 */
	public FileSize getSize(User user) {
		return filesSize.get(user);
	}

	/**
	 * Add a new FileSize to the Map.
	 * 
	 * @param user
	 *            The User.
	 */
	public void addFileSize(User user) {
		this.filesSize.put(user, new FileSize());
		fireFileSizeChanged(user);
	}

	/**
	 * Removes a FileSize from the Map.
	 * 
	 * @param user
	 *            The User.
	 */
	public void removeFileSize(User user) {
		this.filesSize.remove(user);
		fireFileSizeRemoved(user);
	}

	/**
	 * Removes all the FileSizes.
	 */
	public void removeAllFileSizes() {
		for (User u : this.filesSize.keySet()) {
			this.filesSize.remove(u);
			fireFileSizeRemoved(u);
		}
	}

	/**
	 * Sets the file's size.
	 * 
	 * @param user
	 *            The User.
	 * @param size
	 *            Long of the file's size.
	 * @throws UnfindableFileSizeException
	 *             Because of the concurrency.
	 */
	public void setSize(User user, long size)
			throws UnfindableFileSizeException {
		FileSize fileSize = this.filesSize.get(user);
		if (fileSize == null)
			throw new UnfindableFileSizeException(user);
		fileSize.setSize(size);
		fireFileSizeChanged(user);
	}

	/**
	 * Adds the file's size to the current.
	 * 
	 * @param user
	 *            The User.
	 * @param size
	 *            Long of the file's size.
	 * @throws UnfindableFileSizeException
	 *             Because of the concurrency.
	 */
	public void addSize(User user, long size)
			throws UnfindableFileSizeException {
		FileSize fileSize = this.filesSize.get(user);
		if (fileSize == null)
			throw new UnfindableFileSizeException(user);
		fileSize.add(size);
		fireFileSizeChanged(user);
	}

	/**
	 * Adds a listener to the model.
	 * 
	 * @param listener
	 *            The listener to add.
	 */
	public void addFileSizeListener(FileSizeListener listener) {
		filesizeListeners.add(FileSizeListener.class, listener);
	}

	/**
	 * Removes a listener to the model.
	 * 
	 * @param listener
	 *            The listener to remove.
	 */
	public void removeFileSizeListener(FileSizeListener listener) {
		filesizeListeners.remove(FileSizeListener.class, listener);
	}

	/**
	 * Inform the listeners the FileSize changed.
	 */
	public void fireFileSizeChanged(User user) {
		FileSizeListener[] listenerList = filesizeListeners
				.getListeners(FileSizeListener.class);
		for (FileSizeListener listener : listenerList) {
			listener.filesizeChanged(new FileSizeEvent(this, user, this
					.getSize(user)));
		}
	}

	/**
	 * Inform the listeners the FileSize has been removed.
	 */
	public void fireFileSizeRemoved(User user) {
		FileSizeListener[] listenerList = filesizeListeners
				.getListeners(FileSizeListener.class);
		System.out.println(listenerList.length);
		for (FileSizeListener listener : listenerList) {
			listener.filesizeRemoved(new FileSizeEvent(this, user));
		}
	}

}
