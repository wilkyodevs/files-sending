package gui.model.main;

import java.io.IOException;
import java.net.Socket;

public class User {

	/**
	 * The User's Socket.
	 */
	private Socket socket;
	/**
	 * Identifies the User.
	 */
	private String id;

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            The Identifier.
	 */
	public User(String id) {
		this.id = id;
	}

	public User(Socket socket) {
		this.socket = socket;
		this.id = socket.getInetAddress() + ":" + socket.getPort();
	}

	/**
	 * Gets the identifier.
	 * 
	 * @return String of the Id.
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * Gets the Socket.
	 * 
	 * @return Socket.
	 */
	public Socket getSocket() {
		return this.socket;
	}

	/**
	 * Tries to close the Socket.
	 * 
	 * @throws IOException
	 *             If it failed. Must be catch by the calling method.
	 */
	public void closeSocket() throws IOException {
		if (socket != null) {
			socket.close();
			socket = null;
		}
	}

	@Override
	public String toString() {
		return "User(" + id + ")";
	}
}
