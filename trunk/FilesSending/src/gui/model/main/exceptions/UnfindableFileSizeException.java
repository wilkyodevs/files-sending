package gui.model.main.exceptions;

import gui.model.main.User;

/**
 * Thrown when the model is asked for a FileSize that doesn't exist anymore.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class UnfindableFileSizeException extends Exception {

	/**
	 * Serial Version UID.
	 */
	private static final long serialVersionUID = 1L;

	public UnfindableFileSizeException(User user) {
		super(user.toString());
	}

}
