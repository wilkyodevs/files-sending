package gui.model.main.event;

import java.util.EventListener;

/**
 * EventListener of FileSizeEvent type.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public interface FileSizeListener extends EventListener {

	/**
	 * Called when the FileSize changed.
	 * 
	 * @param event
	 *            Event catched with the new FileSize.
	 */
	public void filesizeChanged(FileSizeEvent event);

	/**
	 * Called when the FileSize has been removed.
	 * 
	 * @param event
	 *            Event catched with the id.
	 */
	public void filesizeRemoved(FileSizeEvent event);
}
