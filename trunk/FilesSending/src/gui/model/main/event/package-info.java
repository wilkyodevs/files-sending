/**
 * The events linked to the popup.
 * 
 * @author Willy FRANÇOIS
 *
 */
package gui.model.main.event;