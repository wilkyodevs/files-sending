package gui.model.main.event;

import gui.model.main.FileSize;
import gui.model.main.User;

import java.util.EventObject;

/**
 * Event for the file's size changes.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class FileSizeEvent extends EventObject {

	/**
	 * Version ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The User linked to the FileSize.
	 */
	private User user;

	/**
	 * The File's size.
	 */
	private FileSize filesize;

	/**
	 * Creates the event.
	 * 
	 * @param source
	 *            Source Object.
	 * @param user
	 *            The User.
	 * @param filesize
	 *            The File's size.
	 */
	public FileSizeEvent(Object source, User user, FileSize filesize) {
		super(source);
		this.user = user;
		this.filesize = filesize;
	}

	/**
	 * Creates the event.
	 * 
	 * @param source
	 *            Source Object.
	 * @param user
	 *            The User.
	 */
	public FileSizeEvent(Object source, User user) {
		super(source);
		this.user = user;
	}

	/**
	 * Returns the new FileSize.
	 * 
	 * @return The new FileSize.
	 */
	public FileSize getNewFileSize() {
		return filesize;
	}

	/**
	 * Returns the User.
	 * 
	 * @return The User.
	 */
	public User getUser() {
		return user;
	}
}
