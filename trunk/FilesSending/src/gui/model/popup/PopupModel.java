package gui.model.popup;

import gui.model.popup.event.LanguageChangedEvent;
import gui.model.popup.event.LanguageListener;
import gui.model.popup.event.PopupWindowStatusChangedEvent;
import gui.model.popup.event.PopupWindowStatusListener;

import javax.swing.event.EventListenerList;

/**
 * The Model managing the help window.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class PopupModel {

	/**
	 * States whether the help window is opened or not.
	 */
	private boolean helpOpened;
	/**
	 * Listeners list.
	 */
	private EventListenerList popupListeners, languageListeners;

	/**
	 * Defines the help window as closed.
	 */
	public PopupModel() {
		this(false);
	}

	/**
	 * Defines the help window with the boolean.
	 * 
	 * @param helpOpened
	 *            Tells weather the help window is opened or not
	 */
	private PopupModel(boolean helpOpened) {
		this.helpOpened = helpOpened;
		this.popupListeners = new EventListenerList();
		this.languageListeners = new EventListenerList();
	}

	/**
	 * Returns the status of the help window.
	 * 
	 * @return the status
	 */
	public boolean isHelpOpened() {
		return this.helpOpened;
	}

	/**
	 * Sets the status of the help window.
	 * 
	 * @param helpOpened
	 *            Next status of the window
	 */
	public void setPopupWindowStatus(boolean helpOpened) {
		if (this.helpOpened != helpOpened) {
			this.helpOpened = helpOpened;
			fireHelpWindowStatusChanged();
		}
	}

	/**
	 * Adds a listener to the model.
	 * 
	 * @param listener
	 *            The listener to add
	 */
	public void addPopupWindowStatusListener(PopupWindowStatusListener listener) {
		popupListeners.add(PopupWindowStatusListener.class, listener);
	}

	/**
	 * Removes a listener to the model.
	 * 
	 * @param listener
	 *            The listener to remove
	 */
	public void removePopupWindowStatusListener(
			PopupWindowStatusListener listener) {
		popupListeners.remove(PopupWindowStatusListener.class, listener);
	}

	/**
	 * Adds a listener to the model.
	 * 
	 * @param listener
	 *            The listener to add
	 */
	public void addLanguageListener(LanguageListener listener) {
		languageListeners.add(LanguageListener.class, listener);
	}

	/**
	 * Removes a listener to the model.
	 * 
	 * @param listener
	 *            The listener to remove
	 */
	public void removeLanguageListener(LanguageListener listener) {
		languageListeners.remove(LanguageListener.class, listener);
	}

	/**
	 * Inform the listeners the status of the help window changed.
	 */
	public void fireHelpWindowStatusChanged() {
		PopupWindowStatusListener[] listenerList = popupListeners
				.getListeners(PopupWindowStatusListener.class);
		for (PopupWindowStatusListener listener : listenerList) {
			listener.popupWindowStatusChanged(new PopupWindowStatusChangedEvent(
					this, isHelpOpened()));
		}
	}

	/**
	 * Inform the listeners the language changed.
	 * 
	 * @param language
	 *            The new language
	 */
	public void fireLanguageChanged(int language) {
		LanguageListener[] listenerList = (LanguageListener[]) languageListeners
				.getListeners(LanguageListener.class);
		for (LanguageListener listener : listenerList) {
			listener.languageChanged(new LanguageChangedEvent(this, language));
		}
	}
}
