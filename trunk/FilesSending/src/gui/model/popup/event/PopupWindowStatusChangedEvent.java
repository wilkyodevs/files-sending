package gui.model.popup.event;

import java.util.EventObject;

/**
 * Event for the popup window's status changes.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class PopupWindowStatusChangedEvent extends EventObject {

	/**
	 * Version ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Tells whether the popup is opened or not.
	 */
	private boolean popupOpened;

	/**
	 * Creates the event.
	 * 
	 * @param source
	 *            Source Object
	 * @param popupOpened
	 *            State of the popup
	 */
	public PopupWindowStatusChangedEvent(Object source, boolean popupOpened) {
		super(source);
		this.popupOpened = popupOpened;
	}

	/**
	 * Returns the new popup window status.
	 * 
	 * @return The new status
	 */
	public boolean getNewPopupWindowStatus() {
		return popupOpened;
	}
}
