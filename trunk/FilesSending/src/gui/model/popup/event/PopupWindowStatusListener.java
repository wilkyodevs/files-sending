package gui.model.popup.event;

import java.util.EventListener;

/**
 * EventListener of PopupWindowStatusChangedEvent type.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public interface PopupWindowStatusListener extends EventListener {

	/**
	 * Called when the popup window's status changed.
	 * 
	 * @param event
	 *            Event catched with the new status
	 */
	public void popupWindowStatusChanged(PopupWindowStatusChangedEvent event);
}
