package gui.model.popup.event;

import java.util.EventObject;

/**
 * Event for the language changes.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class LanguageChangedEvent extends EventObject {

	/**
	 * Version ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The language id.
	 */
	private int language;

	/**
	 * Creates the event.
	 * 
	 * @param source
	 *            Source Object
	 * @param language
	 *            The new language
	 */
	public LanguageChangedEvent(Object source, int language) {
		super(source);
		this.language = language;
	}

	/**
	 * Returns the new language.
	 * 
	 * @return The language id
	 */
	public int getNewLanguage() {
		return this.language;
	}
}
