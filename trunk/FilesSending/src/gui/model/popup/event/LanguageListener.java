package gui.model.popup.event;

import java.util.EventListener;

/**
 * EventListener of LanguageChangedEvent type.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public interface LanguageListener extends EventListener {

	/**
	 * Called when the language changed.
	 * 
	 * @param event
	 *            Event caught.
	 */
	public void languageChanged(LanguageChangedEvent event);
}
