package gui.view.main;

import gui.controller.main.IndexController;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import application.LanguageHelper;

/**
 * The view designed for the end of the program.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class EndingView extends MainView {

	public static final int CODE_SERVER_CLOSED = 1;
	public static final int CODE_CLIENT_DISCONNECTED = 2;
	public static final int CODE_FILE_RECEIVED = 3;
	public static final int CODE_SERVER_OFFLINE = 4;
	public static final int CODE_OTHER_ERROR = 5;

	/**
	 * The JButton to close.
	 */
	private JButton closeButton;
	/**
	 * The JLabel.
	 */
	private JLabel mainLabel;

	/**
	 * Defines the text to display.
	 */
	private int endingType = 0;

	/**
	 * Sets the controller and launch the building of the JFrame.
	 * 
	 * @param controller
	 *            The controller
	 */
	public EndingView(IndexController controller, int endingType) {
		super(controller);
		this.endingType = endingType;
		buildFrame();
	}

	/**
	 * Builds the JFrame with its content.
	 */
	private void buildFrame() {
		super.buildFrame(LanguageHelper.getText(LanguageHelper.END));

		GridBagLayout layout = new GridBagLayout();
		frame.setLayout(layout);
		GridBagConstraints contraintes;

		mainLabel = new JLabel();
		contraintes = new GridBagConstraints(0, 0, 1, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(
						15, 10, 5, 10), 0, 0);
		frame.add(mainLabel, contraintes);

		closeButton = new JButton(LanguageHelper.getText(LanguageHelper.CLOSE));
		closeButton.addActionListener(this);
		contraintes = new GridBagConstraints(0, 1, 1, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(
						15, 40, 10, 5), 0, 0);
		frame.add(closeButton, contraintes);

		repack();
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@Override
	public void refresh() {
		super.refresh();
		frame.setTitle(LanguageHelper.getText(LanguageHelper.END));
		switch (endingType) {
		case (CODE_SERVER_CLOSED):
			mainLabel.setText(LanguageHelper
					.getText(LanguageHelper.SERVER_CLOSED_TXT));
			break;
		case (CODE_CLIENT_DISCONNECTED):
			mainLabel.setText(LanguageHelper
					.getText(LanguageHelper.CLIENT_DISCONNECTED_TXT));
			break;
		case (CODE_FILE_RECEIVED):
			mainLabel.setText(LanguageHelper
					.getText(LanguageHelper.FILE_RECEIVED_TXT));
			break;
		case (CODE_SERVER_OFFLINE):
			mainLabel.setText(LanguageHelper
					.getText(LanguageHelper.SERVER_OFFLINE_TXT));
			break;
		default:
			mainLabel.setText(LanguageHelper
					.getText(LanguageHelper.OTHER_ERROR_TXT));
		}
		closeButton.setText(LanguageHelper.getText(LanguageHelper.CLOSE));
		repack();
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		if (event.getSource() == closeButton)
			((IndexController) this.controller).closeClicked();
	}

}
