package gui.view.main.client;

import gui.controller.main.client.ReceivingClientController;
import gui.model.main.event.FileSizeEvent;
import gui.model.main.event.FileSizeListener;
import gui.view.main.FileSizePanel;
import gui.view.main.MainView;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import application.LanguageHelper;

/**
 * The view designed for the receiving.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class ReceivingClientView extends MainView implements FileSizeListener {

	/**
	 * The JButton for stopping.
	 */
	private JButton stopButton;
	/**
	 * The FileSizePanel.
	 */
	private FileSizePanel fsPanel;
	/**
	 * The main JLabel.
	 */
	private JLabel mainLabel;
	/**
	 * Will allow repaint only if necessary.
	 */
	private boolean needRepaint;
	/**
	 * Executes a Runnable every 100 milliseconds.
	 */
	private ScheduledExecutorService scheduledExecutor;

	/**
	 * Sets the controller and launch the building of the JFrame.
	 * 
	 * @param receivingClientController
	 *            The controller.
	 */
	public ReceivingClientView(
			ReceivingClientController receivingClientController) {
		super(receivingClientController);
		buildFrame();
	}

	/**
	 * Builds the JFrame with its content.
	 */
	private void buildFrame() {
		super.buildFrame(LanguageHelper.getText(LanguageHelper.RECEIVE));

		GridBagLayout layout = new GridBagLayout();
		frame.setLayout(layout);
		GridBagConstraints contraintes;

		// Header
		mainLabel = new JLabel(
				((ReceivingClientController) this.controller)
						.isConnectedToServer() ? LanguageHelper
						.getText(LanguageHelper.RECEIVING_CLIENT_TEXT)
						: LanguageHelper
								.getText(LanguageHelper.CONNECTING_TO_SERVER_TEXT));
		contraintes = new GridBagConstraints(0, 0, 2, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(
						15, 10, 5, 10), 0, 0);
		frame.add(mainLabel, contraintes);

		// FileSizePanel
		fsPanel = new FileSizePanel();
		contraintes = new GridBagConstraints(0, 1, 1, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(
						15, 0, 10, 10), 0, 0);
		frame.add(fsPanel, contraintes);

		// Button
		stopButton = new JButton(LanguageHelper.getText(LanguageHelper.EXIT));
		stopButton.addActionListener(this);
		contraintes = new GridBagConstraints(0, 2, 1, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(
						15, 0, 10, 10), 0, 0);
		frame.add(stopButton, contraintes);

		repack();
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		scheduledExecutor = Executors.newScheduledThreadPool(1);
		scheduledExecutor.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				if (needRepaint) {
					fsPanel.repaint();
					needRepaint = false;
				}
			}
		}, 100, 100, TimeUnit.MILLISECONDS);
	}

	@Override
	public void dispose() {
		scheduledExecutor.shutdown();
		super.dispose();
	}

	@Override
	public void refresh() {
		super.refresh();
		frame.setTitle(LanguageHelper.getText(LanguageHelper.RECEIVE));
		mainLabel.setText(((ReceivingClientController) this.controller)
				.isConnectedToServer() ? LanguageHelper
				.getText(LanguageHelper.RECEIVING_CLIENT_TEXT) : LanguageHelper
				.getText(LanguageHelper.CONNECTING_TO_SERVER_TEXT));
		stopButton.setText(LanguageHelper.getText(LanguageHelper.EXIT));
		repack();
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		if (event.getSource() == stopButton)
			((ReceivingClientController) this.controller)
					.stopReceivingClicked();
	}

	@Override
	public void filesizeChanged(FileSizeEvent event) {
		// System.out.println("RView.filesizeChanged("
		// + event.getNewFileSize().getSize() + ")");
		fsPanel.setFileSize(event.getNewFileSize());
		this.needRepaint = true;
	}

	@Override
	public void filesizeRemoved(FileSizeEvent event) {
		// TODO Auto-generated method stub
		// System.out.println("RView.filesizeRemoved" + event);
	}

}
