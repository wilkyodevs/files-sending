package gui.view.main.client;

import gui.controller.main.IndexController;
import gui.view.main.MainView;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import application.FilesSending;
import application.LanguageHelper;

/**
 * The view designed for the settings before the receiving.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class ReceivingSettingsView extends MainView {

	/**
	 * The JButtons for the choices.
	 */
	private JButton continueButton, backButton;
	/**
	 * The JLabels.
	 */
	private JLabel mainLabel, addressLabel, sendPortLabel;
	/**
	 * The JTextField for the file's path, the port to open and the packages
	 * size.
	 */
	private JTextField senderAddressTextField, senderPortTextField;

	/**
	 * The borders to switch easily when something is wrong or right.
	 */
	private Border redBorder = BorderFactory.createLineBorder(Color.RED),
			greenBorder = BorderFactory.createLineBorder(Color.GREEN);

	/**
	 * Sets the controller and launch the building of the JFrame.
	 * 
	 * @param indexController
	 *            The controller.
	 */
	public ReceivingSettingsView(IndexController indexController) {
		super(indexController);
		buildFrame();
	}

	/**
	 * Builds the JFrame with its content.
	 */
	private void buildFrame() {
		super.buildFrame(LanguageHelper.getText(LanguageHelper.RECEIVE));

		GridBagLayout layout = new GridBagLayout();
		frame.setLayout(layout);
		GridBagConstraints contraintes;
		MyDocumentListener docListener = new MyDocumentListener();

		mainLabel = new JLabel(
				LanguageHelper.getText(LanguageHelper.RECEIVING_SETTINGS_TEXT));
		contraintes = new GridBagConstraints(0, 0, 2, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(
						15, 10, 5, 10), 0, 0);
		frame.add(mainLabel, contraintes);

		// Sender Address
		addressLabel = new JLabel(
				LanguageHelper.getText(LanguageHelper.SENDER_ADDRESS));
		contraintes = new GridBagConstraints(0, 1, 1, 1, 0, 0,
				GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(
						15, 0, 5, 20), 0, 0);
		frame.add(addressLabel, contraintes);
		senderAddressTextField = new JTextField(
				String.valueOf(FilesSending.options.getSenderAddress()));
		senderAddressTextField.getDocument().addDocumentListener(docListener);
		contraintes = new GridBagConstraints(1, 1, 1, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(
						15, 0, 5, 10), 0, 0);
		frame.add(senderAddressTextField, contraintes);

		// Sender port
		sendPortLabel = new JLabel(
				LanguageHelper.getText(LanguageHelper.SENDER_PORT));
		contraintes = new GridBagConstraints(0, 2, 1, 1, 0, 0,
				GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(
						15, 0, 5, 20), 0, 0);
		frame.add(sendPortLabel, contraintes);
		senderPortTextField = new JTextField(
				String.valueOf(FilesSending.options.getSenderPort()));
		senderPortTextField.getDocument().addDocumentListener(docListener);
		contraintes = new GridBagConstraints(1, 2, 1, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(
						15, 0, 5, 10), 0, 0);
		frame.add(senderPortTextField, contraintes);

		// Buttons
		backButton = new JButton(LanguageHelper.getText(LanguageHelper.BACK));
		backButton.addActionListener(this);
		contraintes = new GridBagConstraints(0, 3, 1, 1, 0, 0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
						15, 40, 10, 5), 0, 0);
		frame.add(backButton, contraintes);

		continueButton = new JButton(
				LanguageHelper.getText(LanguageHelper.CONTINUE));
		enableContinueButton();
		continueButton.addActionListener(this);
		contraintes = new GridBagConstraints(1, 3, 1, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(
						15, 0, 10, 10), 0, 0);
		frame.add(continueButton, contraintes);

		repack();
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		continueButton.requestFocusInWindow();
	}

	@Override
	public void refresh() {
		super.refresh();
		frame.setTitle(LanguageHelper.getText(LanguageHelper.RECEIVE));
		mainLabel.setText(LanguageHelper
				.getText(LanguageHelper.RECEIVING_SETTINGS_TEXT));
		addressLabel.setText(LanguageHelper
				.getText(LanguageHelper.SENDER_ADDRESS));
		sendPortLabel.setText(LanguageHelper
				.getText(LanguageHelper.SENDER_PORT));
		backButton.setText(LanguageHelper.getText(LanguageHelper.BACK));
		continueButton.setText(LanguageHelper.getText(LanguageHelper.CONTINUE));
		repack();
	}

	private void enableContinueButton() {
		boolean enabled = true;
		if (senderAddressTextField.getText().length() > 0) {
			senderAddressTextField.setToolTipText(null);
			senderAddressTextField.setBorder(greenBorder);
			FilesSending.options.setSenderAddress(senderAddressTextField
					.getText());
		} else {
			senderAddressTextField.setToolTipText(LanguageHelper
					.getText(LanguageHelper.SENDER_ADDRESS_ERROR));
			senderAddressTextField.setBorder(redBorder);
			enabled = false;
		}

		try {
			int port = Integer.parseInt(senderPortTextField.getText());
			if (port > 0 || port <= 65536) {
				senderPortTextField.setToolTipText(null);
				senderPortTextField.setBorder(greenBorder);
				FilesSending.options.setSenderPort(port);
			} else {
				senderPortTextField.setToolTipText(LanguageHelper
						.getText(LanguageHelper.SENDER_PORT_ERROR));
				senderPortTextField.setBorder(redBorder);
				enabled = false;
			}
		} catch (java.lang.NumberFormatException e) {
			senderPortTextField.setToolTipText(LanguageHelper
					.getText(LanguageHelper.SENDER_PORT_ERROR));
			senderPortTextField.setBorder(redBorder);
			enabled = false;
		}
		continueButton.setEnabled(enabled);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		if (event.getSource() == continueButton)
			((IndexController) this.controller).continueClicked();
		else if (event.getSource() == backButton)
			((IndexController) this.controller).backClicked();
	}

	private class MyDocumentListener implements DocumentListener {
		@Override
		public void changedUpdate(DocumentEvent event) {
			enableContinueButton();
		}

		@Override
		public void insertUpdate(DocumentEvent event) {
			enableContinueButton();
		}

		@Override
		public void removeUpdate(DocumentEvent event) {
			enableContinueButton();
		}
	}

}
