package gui.view.main;

import gui.controller.main.MainController;
import gui.model.popup.event.PopupWindowStatusChangedEvent;
import gui.model.popup.event.PopupWindowStatusListener;
import gui.view.View;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import application.LanguageHelper;

/**
 * Superclass for the view where the Help window can be opened.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public abstract class MainView extends View implements
		PopupWindowStatusListener, ActionListener {

	/**
	 * The menu bar.
	 */
	private JMenuBar menuBar = new JMenuBar();
	/**
	 * The file menu.
	 */
	private JMenu fileMenu = new JMenu(
			LanguageHelper.getText(LanguageHelper.FILE));
	/**
	 * The send menu item from the file menu.
	 */
	private JMenuItem sendMenuItem = new JMenuItem(
			LanguageHelper.getText(LanguageHelper.SEND));
	/**
	 * The receive menu item from the file menu.
	 */
	private JMenuItem receiveMenuItem = new JMenuItem(
			LanguageHelper.getText(LanguageHelper.RECEIVE));
	/**
	 * The exit menu item from the file menu.
	 */
	private JMenuItem exitMenuItem = new JMenuItem(
			LanguageHelper.getText(LanguageHelper.EXIT));
	/**
	 * The edit menu.
	 */
	private JMenu editMenu = new JMenu(
			LanguageHelper.getText(LanguageHelper.EDIT));
	/**
	 * The options menu item from the edit menu.
	 */
	private JMenuItem optionsMenuItem = new JMenuItem(
			LanguageHelper.getText(LanguageHelper.OPTIONS));
	/**
	 * The about menu.
	 */
	private JMenu aboutMenu = new JMenu(
			LanguageHelper.getText(LanguageHelper.ABOUT));
	/**
	 * The help menu item from the about menu.
	 */
	private JMenuItem helpMenuItem = new JMenuItem(
			LanguageHelper.getText(LanguageHelper.HELP));

	/**
	 * The controller.
	 */
	protected MainController controller;

	/**
	 * Sets the Controller.
	 * 
	 * @param controller
	 *            The PopupAwareController to set
	 */
	public MainView(MainController controller) {
		this.controller = controller;
	}

	/**
	 * Initialize the JMenuBar.
	 */
	protected void buildFrame(String title) {
		super.buildFrame(title);
		sendMenuItem.addActionListener(this);
		receiveMenuItem.addActionListener(this);
		exitMenuItem.addActionListener(this);
		optionsMenuItem.addActionListener(this);
		helpMenuItem.addActionListener(this);

		fileMenu.add(sendMenuItem);
		fileMenu.add(receiveMenuItem);
		fileMenu.add(exitMenuItem);
		menuBar.add(fileMenu);

		editMenu.add(optionsMenuItem);
		menuBar.add(editMenu);

		aboutMenu.add(helpMenuItem);
		menuBar.add(aboutMenu);

		frame.setJMenuBar(menuBar);
	}

	@Override
	public void refresh() {
		fileMenu.setText(LanguageHelper.getText(LanguageHelper.FILE));
		sendMenuItem.setText(LanguageHelper.getText(LanguageHelper.SEND));
		receiveMenuItem.setText(LanguageHelper.getText(LanguageHelper.RECEIVE));
		exitMenuItem.setText(LanguageHelper.getText(LanguageHelper.EXIT));
		editMenu.setText(LanguageHelper.getText(LanguageHelper.EDIT));
		optionsMenuItem.setText(LanguageHelper.getText(LanguageHelper.OPTIONS));
		aboutMenu.setText(LanguageHelper.getText(LanguageHelper.ABOUT));
		helpMenuItem.setText(LanguageHelper.getText(LanguageHelper.HELP));
		frame.setResizable(true);
		frame.setMinimumSize(new Dimension(0, 0));
	}

	@Override
	public void display() {
		frame.setVisible(true);
	}

	@Override
	public void dispose() {
		if (frame.isVisible())
			frame.dispose();
	}

	/**
	 * Enables the view or not.
	 * 
	 * @param enabled
	 *            Boolean telling whether the view must be enabled or not.
	 */
	public void setEnabled(boolean enabled) {
		frame.setEnabled(enabled);
		// Was hiding without this
		if (enabled)
			display();
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		if (event.getSource() == sendMenuItem)
			((MainController) this.controller).sendClicked();
		else if (event.getSource() == receiveMenuItem)
			((MainController) this.controller).receiveClicked();
		else if (event.getSource() == exitMenuItem)
			((MainController) this.controller).exitClicked();
		else if (event.getSource() == optionsMenuItem)
			((MainController) this.controller).optionsClicked();
		else if (event.getSource() == helpMenuItem)
			((MainController) this.controller).helpClicked();
	}

	@Override
	public void popupWindowStatusChanged(PopupWindowStatusChangedEvent event) {
		setEnabled(!event.getNewPopupWindowStatus());
	}
}
