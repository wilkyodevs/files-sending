package gui.view.main;

import gui.controller.main.IndexController;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import application.LanguageHelper;

/**
 * The view designed for the choice between sending and receiving a file.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class SendReceiveChoiceView extends MainView {

	/**
	 * The JButtons for the choices.
	 */
	private JButton sendButton, receiveButton;
	/**
	 * The main Label.
	 */
	private JLabel mainLabel;

	/**
	 * Sets the controller and launch the building of the JFrame.
	 * 
	 * @param controller
	 *            The controller
	 */
	public SendReceiveChoiceView(IndexController controller) {
		super(controller);
		buildFrame();
	}

	/**
	 * Builds the JFrame with its content.
	 */
	private void buildFrame() {
		super.buildFrame(LanguageHelper
				.getText(LanguageHelper.SEND_RECEIVE_CHOICE));

		GridBagLayout layout = new GridBagLayout();
		frame.setLayout(layout);
		GridBagConstraints contraintes;

		mainLabel = new JLabel(
				LanguageHelper.getText(LanguageHelper.SEND_RECEIVE_CHOICE_TEXT));
		contraintes = new GridBagConstraints(0, 0, 2, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(
						15, 10, 5, 10), 0, 0);
		frame.add(mainLabel, contraintes);

		sendButton = new JButton(LanguageHelper.getText(LanguageHelper.SEND));
		sendButton.addActionListener(this);
		contraintes = new GridBagConstraints(0, 1, 1, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(
						15, 40, 10, 5), 0, 0);
		frame.add(sendButton, contraintes);

		receiveButton = new JButton(
				LanguageHelper.getText(LanguageHelper.RECEIVE));
		receiveButton.addActionListener(this);
		contraintes = new GridBagConstraints(1, 1, 1, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				new Insets(15, 5, 10, 40), 0, 0);
		frame.add(receiveButton, contraintes);

		repack();
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@Override
	public void refresh() {
		super.refresh();
		frame.setTitle(LanguageHelper
				.getText(LanguageHelper.SEND_RECEIVE_CHOICE));
		mainLabel.setText(LanguageHelper
				.getText(LanguageHelper.SEND_RECEIVE_CHOICE_TEXT));
		sendButton.setText(LanguageHelper.getText(LanguageHelper.SEND));
		receiveButton.setText(LanguageHelper.getText(LanguageHelper.RECEIVE));
		repack();
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		if (event.getSource() == sendButton)
			((IndexController) this.controller).sendClicked();
		else if (event.getSource() == receiveButton)
			((IndexController) this.controller).receiveClicked();
	}

}
