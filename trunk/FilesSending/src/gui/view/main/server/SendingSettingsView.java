package gui.view.main.server;

import gui.controller.main.IndexController;
import gui.view.main.MainView;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import application.FilesSending;
import application.LanguageHelper;

/**
 * The view designed for the settings before the sending.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class SendingSettingsView extends MainView {

	/**
	 * The JButtons for the choices.
	 */
	private JButton continueButton, backButton, fileChooserButton;
	/**
	 * The JTextField for the file's path, the port to open and the packages
	 * size.
	 */
	private JTextField fileTextField, portTextField, packagesSizeTextField;
	/**
	 * The JLabels.
	 */
	private JLabel mainLabel, portLabel, packagesSizeLabel;

	/**
	 * The borders to switch easily when something is wrong or right.
	 */
	private Border redBorder = BorderFactory.createLineBorder(Color.RED),
			greenBorder = BorderFactory.createLineBorder(Color.GREEN);

	/**
	 * Sets the controller and launch the building of the JFrame.
	 * 
	 * @param indexController
	 *            The controller.
	 */
	public SendingSettingsView(IndexController indexController) {
		super(indexController);
		buildFrame();
	}

	/**
	 * Builds the JFrame with its content.
	 */
	private void buildFrame() {
		super.buildFrame(LanguageHelper.getText(LanguageHelper.SEND));

		GridBagLayout layout = new GridBagLayout();
		frame.setLayout(layout);
		GridBagConstraints contraintes;
		MyDocumentListener docListener = new MyDocumentListener();

		mainLabel = new JLabel(
				LanguageHelper.getText(LanguageHelper.SENDING_SETTINGS_TEXT));
		contraintes = new GridBagConstraints(0, 0, 2, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(
						15, 10, 5, 10), 0, 0);
		frame.add(mainLabel, contraintes);

		// File
		fileTextField = new JTextField(FilesSending.options.getFilePath());
		fileTextField.setPreferredSize(new Dimension(300, 24));
		fileTextField.setMinimumSize(fileTextField.getPreferredSize());
		fileTextField.setEditable(false);
		contraintes = new GridBagConstraints(0, 1, 1, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(
						15, 10, 5, 10), 0, 0);
		frame.add(fileTextField, contraintes);

		fileChooserButton = new JButton(
				LanguageHelper.getText(LanguageHelper.CHOOSE_FILE));
		fileChooserButton.addActionListener(this);
		contraintes = new GridBagConstraints(1, 1, 1, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(
						15, 10, 5, 10), 0, 0);
		frame.add(fileChooserButton, contraintes);

		// Send port
		portLabel = new JLabel(LanguageHelper.getText(LanguageHelper.SEND_PORT));
		contraintes = new GridBagConstraints(0, 2, 1, 1, 0, 0,
				GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(
						15, 0, 5, 20), 0, 0);
		frame.add(portLabel, contraintes);
		portTextField = new JTextField(String.valueOf(FilesSending.options
				.getSendPort()));
		portTextField.getDocument().addDocumentListener(docListener);
		contraintes = new GridBagConstraints(1, 2, 1, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(
						15, 0, 5, 10), 0, 0);
		frame.add(portTextField, contraintes);

		// Packages size
		packagesSizeLabel = new JLabel(
				LanguageHelper.getText(LanguageHelper.PACKAGES_SIZE));
		contraintes = new GridBagConstraints(0, 3, 1, 1, 0, 0,
				GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(
						15, 0, 5, 20), 0, 0);
		frame.add(packagesSizeLabel, contraintes);
		packagesSizeTextField = new JTextField(
				String.valueOf(FilesSending.options.getPackagesSize()));
		packagesSizeTextField.getDocument().addDocumentListener(docListener);
		contraintes = new GridBagConstraints(1, 3, 1, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(
						15, 0, 5, 10), 0, 0);
		frame.add(packagesSizeTextField, contraintes);

		// Buttons
		backButton = new JButton(LanguageHelper.getText(LanguageHelper.BACK));
		backButton.addActionListener(this);
		contraintes = new GridBagConstraints(0, 4, 1, 1, 0, 0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
						15, 40, 10, 5), 0, 0);
		frame.add(backButton, contraintes);

		continueButton = new JButton(
				LanguageHelper.getText(LanguageHelper.CONTINUE));
		enableContinueButton();
		continueButton.addActionListener(this);
		contraintes = new GridBagConstraints(1, 4, 1, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(
						15, 0, 10, 10), 0, 0);
		frame.add(continueButton, contraintes);

		repack();
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		continueButton.requestFocusInWindow();
	}

	@Override
	public void refresh() {
		super.refresh();
		frame.setTitle(LanguageHelper.getText(LanguageHelper.SEND));
		mainLabel.setText(LanguageHelper
				.getText(LanguageHelper.SENDING_SETTINGS_TEXT));
		fileChooserButton.setText(LanguageHelper
				.getText(LanguageHelper.CHOOSE_FILE));
		portLabel.setText(LanguageHelper.getText(LanguageHelper.SEND_PORT));
		packagesSizeLabel.setText(LanguageHelper
				.getText(LanguageHelper.PACKAGES_SIZE));
		backButton.setText(LanguageHelper.getText(LanguageHelper.BACK));
		continueButton.setText(LanguageHelper.getText(LanguageHelper.CONTINUE));
		repack();
		enableContinueButton();
	}

	/**
	 * Enables the continue button if the fields are corrects.
	 */
	private void enableContinueButton() {
		boolean enabled = true;
		if (FilesSending.options.getFilePath() != null) {
			File f = new File(FilesSending.options.getFilePath());
			if (f.isFile() && f.exists()) {
				fileTextField.setToolTipText(null);
				fileTextField.setBorder(greenBorder);
			} else {
				fileTextField.setToolTipText(LanguageHelper
						.getText(LanguageHelper.CHOOSE_FILE_ERROR));
				fileTextField.setBorder(redBorder);
				enabled = false;
			}
		} else {
			fileTextField.setToolTipText(LanguageHelper
					.getText(LanguageHelper.CHOOSE_FILE_ERROR));
			fileTextField.setBorder(redBorder);
			enabled = false;
		}
		try {
			int port = Integer.parseInt(portTextField.getText());
			if (port > 0 || port <= 65536) {
				portTextField.setToolTipText(null);
				portTextField.setBorder(greenBorder);
				FilesSending.options.setSendPort(port);
			} else {
				packagesSizeTextField.setToolTipText(LanguageHelper
						.getText(LanguageHelper.SEND_PORT_ERROR));
				portTextField.setBorder(redBorder);
				enabled = false;
			}
		} catch (java.lang.NumberFormatException e) {
			portTextField.setToolTipText(LanguageHelper
					.getText(LanguageHelper.SEND_PORT_ERROR));
			portTextField.setBorder(redBorder);
			enabled = false;
		}

		try {
			int size = Integer.parseInt(packagesSizeTextField.getText());
			if (size > 0 || size <= Integer.MAX_VALUE) {
				packagesSizeTextField.setToolTipText(null);
				packagesSizeTextField.setBorder(greenBorder);
				FilesSending.options.setPackagesSize(size);
			} else {
				packagesSizeTextField.setToolTipText(LanguageHelper
						.getText(LanguageHelper.PACKAGES_SIZE_ERROR));
				packagesSizeTextField.setBorder(redBorder);
				enabled = false;
			}
		} catch (java.lang.NumberFormatException e) {
			packagesSizeTextField.setToolTipText(LanguageHelper
					.getText(LanguageHelper.PACKAGES_SIZE_ERROR));
			packagesSizeTextField.setBorder(redBorder);
			enabled = false;
		}
		continueButton.setEnabled(enabled);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		if (event.getSource() == continueButton)
			((IndexController) this.controller).continueClicked();
		else if (event.getSource() == backButton)
			((IndexController) this.controller).backClicked();
		else if (event.getSource() == fileChooserButton) {
			JFileChooser fileChooser = new JFileChooser(
					FilesSending.options.getFilePath());
			if (fileChooser.showOpenDialog(frame) == 0) {
				File choice = fileChooser.getSelectedFile();
				if (choice != null) {
					FilesSending.options.setFilePath(choice.getAbsolutePath());
					fileTextField.setText(choice.getAbsolutePath());
					enableContinueButton();
				}
			}
		}
	}

	private class MyDocumentListener implements DocumentListener {
		@Override
		public void changedUpdate(DocumentEvent event) {
			enableContinueButton();
		}

		@Override
		public void insertUpdate(DocumentEvent event) {
			enableContinueButton();
		}

		@Override
		public void removeUpdate(DocumentEvent event) {
			enableContinueButton();
		}
	}

}
