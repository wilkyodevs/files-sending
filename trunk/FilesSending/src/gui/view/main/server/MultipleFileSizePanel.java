package gui.view.main.server;

import gui.model.main.FileSize;
import gui.model.main.User;
import gui.view.main.FileSizePanel;

import java.awt.GridLayout;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JPanel;

/**
 * Displays multiple FileSizes.
 * 
 * @author Willy FRANÇOIS
 */
public class MultipleFileSizePanel extends JPanel {

	/**
	 * Serial Version UID.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * The FileSize.
	 */
	private Map<User, FileSizePanel> fileSizePanels;

	/**
	 * Initializes the FileSizePanel with a null FileSize.
	 */
	public MultipleFileSizePanel() {
		this.fileSizePanels = new HashMap<User, FileSizePanel>();
		this.setLayout(new GridLayout(0, 1));
	}

	/**
	 * Add a FileSizePanel.
	 * 
	 * @param user
	 *            User linked to the fileSize.
	 * @param fileSize
	 *            FileSize linked to the User.
	 */
	public void addFileSize(User user, FileSize fileSize) {
		FileSizePanel fsp = new FileSizePanel(fileSize);
		fileSizePanels.put(user, fsp);
		this.add(fsp);
	}

	/**
	 * Sets the FileSize.
	 * 
	 * @param fileSize
	 *            The FileSize.
	 */
	public void setFileSize(User user, FileSize fileSize) {
		FileSizePanel fsp = this.fileSizePanels.get(user);
		fsp.setFileSize(fileSize);
		// fsp.repaint();
	}

	/**
	 * Removes the FileSizePanel linked to the corresponding User.
	 * 
	 * @param user
	 *            The User.
	 */
	public void remove(User user) {
		this.remove(fileSizePanels.get(user));
	}

}
