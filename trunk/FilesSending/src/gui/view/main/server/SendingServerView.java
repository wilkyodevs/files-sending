package gui.view.main.server;

import gui.controller.main.server.SendingServerController;
import gui.model.main.FileSize;
import gui.model.main.User;
import gui.model.main.event.FileSizeEvent;
import gui.model.main.event.FileSizeListener;
import gui.view.main.MainView;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import application.LanguageHelper;

/**
 * The view designed for the sending.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class SendingServerView extends MainView implements FileSizeListener {

	/**
	 * The JButton for stopping.
	 */
	private JButton stopButton;
	/**
	 * The main JLabel.
	 */
	private JLabel mainLabel, addressesLabel;
	/**
	 * The MultipleFileSizePanel.
	 */
	private MultipleFileSizePanel mFsPanel;
	/**
	 * Will allow repaint only if necessary.
	 */
	private boolean needRepaint;
	/**
	 * Executes a Runnable every 100 milliseconds.
	 */
	private ScheduledExecutorService scheduledExecutor;

	/**
	 * Sets the controller and launch the building of the JFrame.
	 * 
	 * @param sendingServerController
	 *            The controller.
	 */
	public SendingServerView(SendingServerController sendingServerController) {
		super(sendingServerController);
		buildFrame();
	}

	/**
	 * Builds the JFrame with its content.
	 */
	private void buildFrame() {
		super.buildFrame(LanguageHelper.getText(LanguageHelper.SEND));

		GridBagLayout layout = new GridBagLayout();
		frame.setLayout(layout);
		GridBagConstraints contraintes;

		// Header
		mainLabel = new JLabel(
				LanguageHelper.getText(LanguageHelper.SENDING_SERVER_TEXT));
		contraintes = new GridBagConstraints(0, 0, 1, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(
						15, 10, 5, 10), 0, 0);
		frame.add(mainLabel, contraintes);

		// Addresses
		try {
			addressesLabel = new JLabel(InetAddress.getLocalHost()
					.getHostAddress().toString());
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		contraintes = new GridBagConstraints(0, 1, 1, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(
						10, 10, 5, 10), 0, 0);
		frame.add(addressesLabel, contraintes);

		// FilesSize Panels
		mFsPanel = new MultipleFileSizePanel();
		contraintes = new GridBagConstraints(0, 2, 1, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(
						15, 0, 10, 10), 0, 0);
		frame.add(mFsPanel, contraintes);

		// Button
		stopButton = new JButton(LanguageHelper.getText(LanguageHelper.EXIT));
		stopButton.addActionListener(this);
		contraintes = new GridBagConstraints(0, 3, 1, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(
						15, 0, 10, 10), 0, 0);
		frame.add(stopButton, contraintes);

		repack();
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		scheduledExecutor = Executors.newScheduledThreadPool(1);
		scheduledExecutor.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				if (needRepaint) {
					mFsPanel.repaint();
					needRepaint = false;
				}
			}
		}, 100, 100, TimeUnit.MILLISECONDS);
	}

	@Override
	public void refresh() {
		super.refresh();
		frame.setTitle(LanguageHelper.getText(LanguageHelper.SEND));
		mainLabel.setText(LanguageHelper
				.getText(LanguageHelper.SENDING_SERVER_TEXT));
		stopButton.setText(LanguageHelper.getText(LanguageHelper.EXIT));
		repack();
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		if (event.getSource() == stopButton)
			((SendingServerController) this.controller).stopSendingClicked();
	}

	/**
	 * Add a FileSizePanel to the MultipleFileSizePanel.
	 * 
	 * @param user
	 *            The User linked to the FileSize.
	 * @param filesize
	 *            The FileSize linked to the User.
	 */
	public void filesizeAdded(User user, FileSize filesize) {
		mFsPanel.addFileSize(user, filesize);
		repack();
		repack();
	}

	@Override
	public void filesizeChanged(FileSizeEvent event) {
		mFsPanel.setFileSize(event.getUser(), event.getNewFileSize());
		this.needRepaint = true;
	}

	@Override
	public void filesizeRemoved(FileSizeEvent event) {
		mFsPanel.remove(event.getUser());
		repack();
		repack();
	}

}
