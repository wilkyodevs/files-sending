package gui.view.main;

import gui.model.main.FileSize;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

/**
 * Displays a FileSize.
 * 
 * @author Willy FRANÇOIS
 */
public class FileSizePanel extends JPanel {

	/**
	 * Serial Version UID.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * The FileSize.
	 */
	private FileSize fileSize;
	/**
	 * The Font for the text to draw.
	 */
	private Font font;

	/**
	 * Initializes the FileSizePanel with a null FileSize.
	 */
	public FileSizePanel() {
		this(null);
	}

	/**
	 * Initializes the FileSizePanel with the FileSize.
	 * 
	 * @param fileSize
	 *            The FileSize.
	 */
	public FileSizePanel(FileSize fileSize) {
		super();
		this.setPreferredSize(new Dimension(180, 28));
		this.setMinimumSize(this.getPreferredSize());
		this.fileSize = fileSize;
		this.font = new Font(Font.SANS_SERIF, Font.PLAIN, 12);
	}

	/**
	 * Sets the FileSize.
	 * 
	 * @param fileSize
	 *            The FileSize.
	 */
	public void setFileSize(FileSize fileSize) {
		this.fileSize = fileSize;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (fileSize == null)
			return;
		g.setColor(new Color(238, 238, 238));
		g.fillRect(0, 0, getWidth(), getHeight());
		String size = fileSize.toString();
		g.setFont(font);
		g.setColor(Color.BLACK);
		g.drawString(size, (getWidth() - size.length() * 5) / 2, (getHeight()
				+ font.getSize() - 2) / 2);
	}
}
