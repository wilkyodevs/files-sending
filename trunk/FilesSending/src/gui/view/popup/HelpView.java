package gui.view.popup;

import gui.controller.popup.HelpController;
import gui.view.View;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import application.LanguageHelper;

/**
 * The view designed for the help.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class HelpView extends View implements ActionListener,
		ListSelectionListener {

	/**
	 * List of helps.
	 */
	private JList<String> helpsList;
	private static final int[] labels = { LanguageHelper.HELP_GOAL,
			LanguageHelper.HELP_OPTIONS, LanguageHelper.HELP_BEFORE_SENDING,
			LanguageHelper.HELP_BEFORE_SENDING_FILE,
			LanguageHelper.HELP_BEFORE_SENDING_PORT,
			LanguageHelper.HELP_BEFORE_SENDING_PACKAGES,
			LanguageHelper.HELP_SENDING, LanguageHelper.HELP_BEFORE_RECEIVING,
			LanguageHelper.HELP_BEFORE_RECEIVING_ADDRESS,
			LanguageHelper.HELP_BEFORE_RECEIVING_PORT,
			LanguageHelper.HELP_RECEIVING };
	/**
	 * The right panel.
	 */
	private JLabel rightPanel;
	private static final int[] helps = { LanguageHelper.HELP_GOAL_TEXT,
			LanguageHelper.HELP_OPTIONS_TEXT,
			LanguageHelper.HELP_BEFORE_SENDING_TEXT,
			LanguageHelper.HELP_BEFORE_SENDING_FILE_TEXT,
			LanguageHelper.HELP_BEFORE_SENDING_PORT_TEXT,
			LanguageHelper.HELP_BEFORE_SENDING_PACKAGES_TEXT,
			LanguageHelper.HELP_SENDING_TEXT,
			LanguageHelper.HELP_BEFORE_RECEIVING_TEXT,
			LanguageHelper.HELP_BEFORE_RECEIVING_ADDRESS_TEXT,
			LanguageHelper.HELP_BEFORE_RECEIVING_PORT_TEXT,
			LanguageHelper.HELP_RECEIVING_TEXT };
	/**
	 * The JButtons for the choices.
	 */
	private JButton closeButton;
	/**
	 * The Controller.
	 */
	private HelpController controller;

	/**
	 * Sets the controller and launch the building of the JFrame.
	 * 
	 * @param controller
	 *            The controller
	 */
	public HelpView(HelpController controller) {
		this.controller = controller;
		buildFrame();
	}

	/**
	 * Builds an Array of the helps labels in the current language.
	 * 
	 * @return The String Array.
	 */
	private static String[] getLabels() {
		String[] res = new String[labels.length];
		for (int i = 0; i < res.length; i++) {
			res[i] = LanguageHelper.getText(labels[i]);
		}
		return res;
	}

	/**
	 * Builds the JFrame with its content.
	 */
	private void buildFrame() {
		super.buildFrame(LanguageHelper.getText(LanguageHelper.HELP));

		GridBagLayout layout = new GridBagLayout();
		frame.setLayout(layout);
		GridBagConstraints contraintes;

		// The List
		helpsList = new JList<String>(getLabels());
		helpsList.addListSelectionListener(this);
		JScrollPane listScroll = new JScrollPane(helpsList);
		rightPanel = new JLabel();
		rightPanel.setOpaque(true);
		rightPanel.setBackground(Color.WHITE);
		rightPanel.setBorder(BorderFactory.createEmptyBorder(15, 12, 20, 12));
		rightPanel.setVerticalAlignment(JLabel.TOP);

		JSplitPane split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				listScroll, rightPanel);
		split.setDividerSize(4);
		split.setEnabled(false);
		split.setSize(400, 200);
		split.setPreferredSize(split.getSize());
		contraintes = new GridBagConstraints(0, 0, 1, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(
						15, 20, 10, 20), 0, 0);
		layout.setConstraints(split, contraintes);
		frame.add(split);

		// The Button
		closeButton = new JButton(LanguageHelper.getText(LanguageHelper.CLOSE));
		closeButton.addActionListener(this);
		contraintes = new GridBagConstraints(0, 1, 1, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(
						15, 40, 10, 5), 0, 0);
		layout.setConstraints(closeButton, contraintes);
		frame.add(closeButton);

		super.repack();
		frame.setLocationRelativeTo(null);
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent event) {
				controller.popupWindowClosed();
			}
		});
	}

	@Override
	public void refresh() {
		helpsList.setListData(getLabels());
		super.repack();
	}

	@Override
	public void display() {
		frame.setVisible(true);
	}

	@Override
	public void dispose() {
		frame.dispose();
	}

	/**
	 * Sets the right panel with the selected help.
	 * 
	 * @param index
	 *            Integer of the chosen help.
	 */
	public void setHelp(int index) {
		rightPanel.setText(LanguageHelper.getText(helps[index]));
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		if (event.getSource() == closeButton)
			this.controller.popupWindowClosed();
	}

	@Override
	public void valueChanged(ListSelectionEvent event) {
		if (!event.getValueIsAdjusting())
			controller.labelClicked(((JList<?>) event.getSource())
					.getSelectedIndex());
	}

}
