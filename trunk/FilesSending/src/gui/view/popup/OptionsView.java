package gui.view.popup;

import gui.controller.popup.OptionsController;
import gui.view.View;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JComboBox;

import application.LanguageHelper;

/**
 * The view designed for the help.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class OptionsView extends View implements ActionListener {

	/**
	 * Names of the languages.
	 */
	private String[] languagesStrings = new String[] {
			LanguageHelper.getText(LanguageHelper.ENGLISH),
			LanguageHelper.getText(LanguageHelper.FRENCH) };

	/**
	 * The JButtons for the choices.
	 */
	private JButton saveButton, cancelButton;
	/**
	 * Select Box.
	 */
	private JComboBox<String> languagesBox;
	/**
	 * The Controller.
	 */
	private OptionsController controller;

	/**
	 * Sets the controller and launch the building of the JFrame.
	 * 
	 * @param controller
	 *            The controller
	 */
	public OptionsView(OptionsController controller) {
		this.controller = controller;
		buildFrame();
	}

	/**
	 * Builds the JFrame with its content.
	 */
	private void buildFrame() {
		super.buildFrame(LanguageHelper.getText(LanguageHelper.OPTIONS));

		GridBagLayout layout = new GridBagLayout();
		frame.setLayout(layout);
		GridBagConstraints contraintes;

		languagesBox = new JComboBox<String>(languagesStrings);
		languagesBox.setSelectedIndex(this.controller.getOptions()
				.getLanguage());
		languagesBox.addActionListener(this);
		contraintes = new GridBagConstraints(0, 0, 2, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(
						15, 40, 10, 40), 0, 0);
		layout.setConstraints(languagesBox, contraintes);
		frame.add(languagesBox);

		saveButton = new JButton(LanguageHelper.getText(LanguageHelper.SAVE));
		saveButton.addActionListener(this);
		contraintes = new GridBagConstraints(0, 1, 1, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(
						15, 40, 10, 5), 0, 0);
		layout.setConstraints(saveButton, contraintes);
		frame.add(saveButton);

		cancelButton = new JButton(
				LanguageHelper.getText(LanguageHelper.CANCEL));
		cancelButton.addActionListener(this);
		contraintes = new GridBagConstraints(1, 1, 1, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(
						15, 5, 10, 40), 0, 0);
		layout.setConstraints(cancelButton, contraintes);
		frame.add(cancelButton);

		frame.pack();
		frame.setMinimumSize(frame.getSize());
		frame.setMaximumSize(frame.getSize());
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent event) {
				controller.popupWindowClosed();
			}
		});
	}

	@Override
	public void refresh() {
		frame.setTitle(LanguageHelper.getText(LanguageHelper.OPTIONS));
		saveButton.setText(LanguageHelper.getText(LanguageHelper.SAVE));
		cancelButton.setText(LanguageHelper.getText(LanguageHelper.CANCEL));

		final int index = languagesBox.getSelectedIndex();
		languagesStrings = new String[] {
				LanguageHelper.getText(LanguageHelper.ENGLISH),
				LanguageHelper.getText(LanguageHelper.FRENCH) };
		languagesBox.removeAllItems();
		for (String e : languagesStrings)
			languagesBox.addItem(e);
		languagesBox.setSelectedIndex(index);
		repack();
	}

	@Override
	public void display() {
		frame.setVisible(true);
	}

	@Override
	public void dispose() {
		frame.dispose();
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		if (event.getSource() == languagesBox)
			this.controller.notifyLanguageChanged(languagesBox
					.getSelectedIndex());
		else if (event.getSource() == cancelButton)
			this.controller.popupWindowClosed();
		else if (event.getSource() == saveButton)
			this.controller.saveClicked();
	}
}
