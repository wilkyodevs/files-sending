/**
 * Package of the different views.
 * 
 * @author Willy FRANÇOIS
 *
 */
package gui.view;