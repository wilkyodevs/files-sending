package gui.view;

import java.awt.Dimension;

import javax.swing.JFrame;

/**
 * Superclass for all the views.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public abstract class View {

	/**
	 * The JFrame which represents the view.
	 */
	protected JFrame frame;

	/**
	 * Initialize the JFrame with its title.
	 */
	protected void buildFrame(String title) {
		frame = new JFrame(title);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	/**
	 * Pack the JFrame.
	 */
	protected void repack() {
		frame.setResizable(true);
		frame.setMinimumSize(new Dimension(0, 0));
		frame.pack();
		frame.setMinimumSize(frame.getSize());
		frame.setMaximumSize(frame.getSize());
		frame.setResizable(false);
	}

	/**
	 * Refreshes the View.
	 */
	public abstract void refresh();

	/**
	 * Shows the view.
	 */
	public abstract void display();

	/**
	 * Releases all of the native screen resources used by this View.
	 */
	public abstract void dispose();

}
