package gui.controller;

/**
 * Superclass for all the controllers.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public abstract class Controller {

	/**
	 * Displays the View.
	 */
	protected abstract void displayView();

	/**
	 * Disposes the View.
	 */
	protected abstract void disposeView();

}
