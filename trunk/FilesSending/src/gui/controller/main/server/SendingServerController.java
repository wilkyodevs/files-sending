package gui.controller.main.server;

import gui.controller.main.IndexController;
import gui.controller.main.MainController;
import gui.model.main.FileSizeModel;
import gui.model.main.User;
import gui.model.main.exceptions.UnfindableFileSizeException;
import gui.model.popup.PopupModel;
import gui.model.popup.event.LanguageChangedEvent;
import gui.view.main.EndingView;
import gui.view.main.client.ReceivingSettingsView;
import gui.view.main.server.SendingServerView;
import gui.view.main.server.SendingSettingsView;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * The controller for the sending. Manages the connections to the server.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class SendingServerController extends MainController {

	/**
	 * The FileSizeModel.
	 */
	private FileSizeModel fsModel;
	/**
	 * The view.
	 */
	private SendingServerView view;
	/**
	 * ExecutorService managing the listening server.
	 */
	private ExecutorService listeningServerExecutor;
	/**
	 * The class to collect the clients.
	 */
	private ConnectionServer server;

	/**
	 * Creates and displays the view.
	 */
	public SendingServerController(PopupModel model) {
		super(model);
		fsModel = new FileSizeModel();
		this.view = new SendingServerView(this);
		this.fsModel.addFileSizeListener(this.view);
		displayView();
		server = new ConnectionServer(this);
		listeningServerExecutor = Executors.newSingleThreadExecutor();
		listeningServerExecutor.execute(server);
	}

	@Override
	public void displayView() {
		if (viewDisposed) {
			viewDisposed = false;
			addLanguageListener();
			addPopupWindowStatusListener(this.view);
			this.view.display();
		}
	}

	@Override
	public void disposeView() {
		if (!viewDisposed) {
			viewDisposed = true;
			removeLanguageListener();
			removePopupWindowStatusListener(this.view);
			this.view.dispose();
		}
	}

	/**
	 * Closes the server.
	 */
	public void stopSendingClicked() {
		sendingServerStopped();
		processNextController(EndingView.CODE_SERVER_CLOSED);
	}

	/**
	 * Called when a client is connecting to the server.
	 * 
	 * @param client
	 *            The User linked to the client.
	 */
	public void clientConnected(User client) {
		System.out.println("Connexion d'un Client." + client.getId());
		view.filesizeAdded(client, null);
		fsModel.addFileSize(client);
	}

	/**
	 * Called when data has been sent to a client.
	 * 
	 * @param clientSocket
	 *            The Socket to the client.
	 * @param length
	 *            The length of the sent package.
	 */
	public void sentData(User user, long length) {
		try {
			fsModel.addSize(user, length);
		} catch (UnfindableFileSizeException e) {
			e.printStackTrace();
			sendingServerStopped();
			// processNextController(4);
		}
	}

	/**
	 * Called when the sending has terminated for a client.
	 * 
	 * @param user
	 *            The User linked to the client.
	 * @param success
	 *            Tells whether the sending has finished with success or not.
	 */
	public void sendingTerminated(User user, boolean success) {
		fsModel.removeFileSize(user);
		if (success)
			System.out.println(user.getId() + " transmitted");
		else
			System.out.println(user.getId() + " error");
	}

	/**
	 * Removes an User from the ConnectionServer.
	 * 
	 * @param user
	 *            User to remove.
	 */
	public void receivingClientStopped(User user) {
		server.stopped(user);
	}

	/**
	 * Called when the SendingServer has terminated.
	 */
	private void sendingServerStopped() {
		System.out.println("Stopped");
		server.stop();
		listeningServerExecutor.shutdownNow();
		fsModel.removeAllFileSizes();
		disposeView();
		System.out.println("StoppedEnd");
	}

	@Override
	public void sendClicked() {
		sendingServerStopped();
		new IndexController(popupModel, SendingSettingsView.class);
	}

	@Override
	public void receiveClicked() {
		sendingServerStopped();
		new IndexController(popupModel, ReceivingSettingsView.class);
	}

	@Override
	public void languageChanged(LanguageChangedEvent event) {
		view.refresh();
	}

}
