package gui.controller.main.server;

import gui.model.main.User;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.concurrent.Semaphore;

import application.FilesSending;

/**
 * Manages the sending of the file to a Client.
 * 
 * @author Willy FRANÇOIS.
 * 
 */
public class SendingUtility implements Runnable {

	/**
	 * The Controller.
	 */
	private SendingServerController controller;
	/**
	 * The Client User.
	 */
	private User client;
	/**
	 * The File Pointer.
	 */
	private RandomAccessFile filePointer;
	/**
	 * Stream of data to the client.
	 */
	private DataOutputStream clientOutputStream;
	/**
	 * Stream of data from the client.
	 */
	private DataInputStream clientInputStream;
	/**
	 * Semaphore for concurrency.
	 */
	private Semaphore mutex;

	/**
	 * Retrieves the client socket.
	 * 
	 * @param controller
	 *            The Controller.
	 * 
	 * @param client
	 *            The User linked to the client.
	 */
	public SendingUtility(SendingServerController controller, User client) {
		this.controller = controller;
		this.client = client;
		this.mutex = new Semaphore(1);
	}

	/**
	 * Force the acquire of the Semaphore.
	 */
	private void forcedAcquire() {
		boolean fail;
		do {
			fail = false;
			try {
				mutex.acquire();
			} catch (InterruptedException e) {
				fail = true;
			}
		} while (fail);
	}

	@Override
	public void run() {
		System.out.println(client.getId() + " connected");
		try {
			// Creation of the reading stream
			filePointer = new RandomAccessFile(new File(
					FilesSending.options.getFilePath()), "r");

			byte[] b = new byte[FilesSending.options.getPackagesSize()], aux;
			int length;

			clientOutputStream = new DataOutputStream(client.getSocket()
					.getOutputStream());

			// Sending of the filename
			String[] sp = FilesSending.options.getFilePath().split("[/\\\\]");
			clientOutputStream.writeUTF(sp[sp.length - 1]);

			clientInputStream = new DataInputStream(client.getSocket()
					.getInputStream());

			// Getting the starting pointer
			final long start = clientInputStream.readLong();
			if (start <= filePointer.length())
				filePointer.seek(start);
			else
				System.err.println("WHAT ?"); // TODO Launch Exception

			controller.sentData(client, start);

			clientOutputStream.writeLong(filePointer.length());

			// Sending loop
			while ((length = filePointer.read(b)) != -1) { // May be null...
				forcedAcquire();
				if (client.getSocket() == null) {
					mutex.release();
					return;
				}
				// Length of the sent package
				controller.sentData(client, length);
				aux = Arrays.copyOf(b, length);
				clientOutputStream.write(aux);
				mutex.release();
			}

			controller.sendingTerminated(client, true);
		} catch (IOException e) {
			System.err.println("Connexion perdue.");
			mutex.release();
			controller.sendingTerminated(client, false);
		}
		stop();
	}

	public void stop() {
		if (this.client.getSocket() != null) {
			try {
				forcedAcquire(); // Foire quand le client avait rage quit
				controller.receivingClientStopped(client);
				if (filePointer != null) {
					filePointer.close();
					filePointer = null;
					System.out.println("Flux de lecture du fichier clos.");
				}
				if (clientInputStream != null) {
					clientInputStream.close();
					clientInputStream = null;
					System.out
							.println("Flux de réception depuis le Client clos.");
				}
				if (clientOutputStream != null) {
					clientOutputStream.close();
					clientOutputStream = null;
					System.out.println("Flux d'envoi vers le Client clos.");
				}
				client.closeSocket();
			} catch (IOException e) {
				e.printStackTrace();
			}
			mutex.release();
		}
	}

}
