package gui.controller.main.server;

import gui.model.main.User;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import application.FilesSending;

/**
 * Manages the arriving clients on the Server.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class ConnectionServer implements Runnable {

	/**
	 * The ServerSocket waiting for clients to connect.
	 */
	private ServerSocket listeningSocket;
	/**
	 * The Controller.
	 */
	private SendingServerController controller;
	/**
	 * ExecutorService managing the sendings.
	 */
	private ExecutorService sendingUtilitiesExecutor;
	/**
	 * Map of the SendingUtilities for each client.
	 */
	private Map<User, SendingUtility> sendingUtilities;

	/**
	 * Sets the Controller.
	 * 
	 * @param controller
	 *            The SendingServerController to set.
	 */
	public ConnectionServer(SendingServerController controller) {
		this.controller = controller;
		sendingUtilities = new HashMap<User, SendingUtility>();
	}

	@Override
	public void run() {
		try {
			File test = new File(FilesSending.options.getFilePath());
			if (!test.exists()) {
				System.err.println("Le fichier " + test.getAbsolutePath()
						+ " n'existe pas !");
				System.exit(5);
			}
			if (!test.isFile()) {
				System.err.println(test.getAbsolutePath()
						+ " n'est pas un fichier");
				System.exit(6);
			}

			System.out.println("Ouverture du port "
					+ FilesSending.options.getSendPort() + "...");
			listeningSocket = new ServerSocket(
					FilesSending.options.getSendPort());
			System.out.println("Port ouvert.");

			sendingUtilitiesExecutor = Executors.newCachedThreadPool();
			try {
				while (true) {
					// We are constantly waiting for clients to connect
					User client = new User(listeningSocket.accept());
					controller.clientConnected(client);
					SendingUtility sender = new SendingUtility(controller,
							client);
					sendingUtilities.put(client, sender);
					sendingUtilitiesExecutor.execute(sender);
				} // Let's loop back to wait for a new connection
			} catch (IOException e) {
				System.err.println("Server Socket closed");
				// controller.sendingServerTerminated();
			}
		} catch (IOException e) {
			System.out.println("Le port " + FilesSending.options.getSendPort()
					+ " n'est pas accessible !");
			System.exit(7);
		}
	}

	/**
	 * Removes an User from the Map.
	 * 
	 * @param user
	 *            User to remove.
	 */
	public void stopped(User user) {
		sendingUtilities.remove(user);
	}

	/**
	 * Stops the Thread properly.
	 */
	public void stop() {
		try {
			if (listeningSocket != null) {
				listeningSocket.close();
				listeningSocket = null;
			}
			for (SendingUtility su : sendingUtilities.values()) {
				su.stop();
			}
			sendingUtilitiesExecutor.shutdownNow();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
