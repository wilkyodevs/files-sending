package gui.controller.main;

import gui.controller.Controller;
import gui.controller.popup.HelpController;
import gui.controller.popup.OptionsController;
import gui.model.popup.PopupModel;
import gui.model.popup.event.LanguageChangedEvent;
import gui.model.popup.event.LanguageListener;
import gui.view.main.MainView;

/**
 * SuperClass for the controllers aware of the HelpModel.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public abstract class MainController extends Controller implements
		LanguageListener {

	/**
	 * The view.
	 */
	protected MainView view;
	/**
	 * Tells whether the view is disposed or not.
	 */
	protected boolean viewDisposed;
	/**
	 * The popup model.
	 */
	protected PopupModel popupModel;
	/**
	 * Tells whether the next Controller has been launched or not.
	 */
	private boolean routeDone = false;

	/**
	 * Constructor which sets the model.
	 * 
	 * @param model
	 *            The HelpModel to set
	 */
	public MainController(PopupModel model) {
		this.popupModel = model;
		this.viewDisposed = true;
	}

	@Override
	protected void displayView() {
		if (viewDisposed) {
			viewDisposed = false;
			addLanguageListener();
			addPopupWindowStatusListener(this.view);
			this.view.display();
		}
	}

	@Override
	protected void disposeView() {
		if (!viewDisposed) {
			viewDisposed = true;
			removeLanguageListener();
			removePopupWindowStatusListener(this.view);
			this.view.dispose();
		}
	}

	/**
	 * Launches the next Controller if it hasn't already be done.
	 * 
	 * @param endingType
	 *            Integer of the ending type.
	 */
	protected void processNextController(int endingType) {
		if (!routeDone) {
			routeDone = true;
			new IndexController(popupModel, endingType);
		}
	}

	@Override
	public void languageChanged(LanguageChangedEvent event) {
		view.refresh();
	}

	/**
	 * Closes the current view and launches the sending one.
	 */
	public abstract void sendClicked();

	/**
	 * Closes the current view and launches the receiving one.
	 */
	public abstract void receiveClicked();

	/**
	 * Closes the application.
	 */
	public void exitClicked() {
		System.exit(0);
	}

	/**
	 * Launches the options view.
	 */
	public void optionsClicked() {
		new OptionsController(popupModel);
	}

	/**
	 * Launches the help view.
	 */
	public void helpClicked() {
		new HelpController(popupModel);
	}

	/**
	 * Adds the view to the listeners of the PopupModel.
	 * 
	 * @param view
	 *            The FilesSendingView to add
	 */
	protected void addPopupWindowStatusListener(MainView view) {
		popupModel.addPopupWindowStatusListener(view);
	}

	/**
	 * Removes the view to the listeners of the PopupModel.
	 * 
	 * @param view
	 *            The FilesSendingView to remove
	 */
	protected void removePopupWindowStatusListener(MainView view) {
		popupModel.removePopupWindowStatusListener(view);
	}

	/**
	 * Add the controller to the listeners of the HelpModel.
	 * 
	 * @param view
	 *            The PopupAwareController to add
	 */
	protected void addLanguageListener() {
		popupModel.addLanguageListener(this);
	}

	/**
	 * Removes the controller to the listener of the PopupModel.
	 * 
	 * @param controller
	 *            The PopupAwareController to remove
	 */
	protected void removeLanguageListener() {
		popupModel.removeLanguageListener(this);
	}

	/**
	 * Tell the model whether the help window is opened or not.
	 * 
	 * @param helpOpened
	 *            New state of the help window
	 */
	public void notifyPopupWindowStatusChanged(boolean helpOpened) {
		popupModel.setPopupWindowStatus(helpOpened);
	}
}
