package gui.controller.main;

import gui.controller.main.client.ReceivingClientController;
import gui.controller.main.server.SendingServerController;
import gui.model.popup.PopupModel;
import gui.view.main.EndingView;
import gui.view.main.MainView;
import gui.view.main.SendReceiveChoiceView;
import gui.view.main.client.ReceivingSettingsView;
import gui.view.main.server.SendingSettingsView;
import application.FilesSending;
import application.OptionsManager;

/**
 * The controller for the firsts views.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class IndexController extends MainController {

	/**
	 * Creates and displays the view.
	 * 
	 * @param model
	 *            The PopupModel.
	 */
	public IndexController(PopupModel model) {
		super(model);
		this.view = new SendReceiveChoiceView(this);
		displayView();
	}

	/**
	 * Creates and display the ending View.
	 * 
	 * @param model
	 *            The PopupModel.
	 * @param endingType
	 *            Defines the text to display.
	 */
	public IndexController(PopupModel model, int endingType) {
		super(model);
		this.view = new EndingView(this, endingType);
		this.view.refresh();
		displayView();
	}

	/**
	 * Creates and displays the specified view.
	 */
	public IndexController(PopupModel model, Class<? extends MainView> classType) {
		super(model);
		if (classType == SendingSettingsView.class)
			this.view = new SendingSettingsView(this);
		else if (classType == ReceivingSettingsView.class)
			this.view = new ReceivingSettingsView(this);
		displayView();
	}

	@Override
	public void sendClicked() {
		disposeView();
		this.view = new SendingSettingsView(this);
		displayView();
	}

	@Override
	public void receiveClicked() {
		disposeView();
		this.view = new ReceivingSettingsView(this);
		displayView();
	}

	/**
	 * Confirms the settings and launch the next view.
	 */
	public void continueClicked() {
		OptionsManager.save(FilesSending.options);
		disposeView();
		if (this.view.getClass() == SendingSettingsView.class)
			new SendingServerController(popupModel);
		else if (this.view.getClass() == ReceivingSettingsView.class)
			new ReceivingClientController(popupModel);
		else
			System.exit(0);
	}

	/**
	 * Closes the application.
	 */
	public void closeClicked() {
		disposeView();
		System.exit(0);
	}

	/**
	 * Closes the current view and launches the previous one.
	 */
	public void backClicked() {
		disposeView();
		this.view = new SendReceiveChoiceView(this);
		displayView();
	}
}
