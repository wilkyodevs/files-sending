package gui.controller.main.client;

import gui.model.main.User;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.Socket;
import java.net.SocketException;
import java.util.Arrays;
import java.util.concurrent.Semaphore;

import application.FilesSending;

/**
 * Manages the arriving clients on the Server.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class ReceivingClient implements Runnable {

	/**
	 * The Controller.
	 */
	private ReceivingClientController controller;
	/**
	 * The link to the Server.
	 */
	private User server;
	/**
	 * The File Pointer.
	 */
	private RandomAccessFile filePointer;
	/**
	 * Stream of data from the server.
	 */
	private DataInputStream serverInputStream;
	/**
	 * Stream of data to the server.
	 */
	private DataOutputStream serverOutputStream;
	/**
	 * Semaphore for concurrency.
	 */
	private Semaphore mutex;

	/**
	 * Sets the Controller.
	 * 
	 * @param controller
	 *            The ReceivingClientController.
	 */
	public ReceivingClient(ReceivingClientController controller) {
		this.controller = controller;
		this.mutex = new Semaphore(1);
	}

	/**
	 * Force the acquire of the Semaphore.
	 */
	private void forcedAcquire() {
		boolean fail;
		do {
			fail = false;
			try {
				mutex.acquire();
			} catch (InterruptedException e) {
				fail = true;
			}
		} while (fail);
	}

	/**
	 * Download the file from the Server.
	 */
	private void download() {
		try {
			serverInputStream = new DataInputStream(server.getSocket()
					.getInputStream());
			byte[] b = new byte[FilesSending.options.getPackagesSize()], aux;
			int length;

			// Getting the filename
			String filename = serverInputStream.readUTF();
			controller.filenameReceived(filename);

			serverOutputStream = new DataOutputStream(server.getSocket()
					.getOutputStream());
			filePointer = new RandomAccessFile(new File(filename), "rw");

			// Sending of the file's length
			filePointer.seek(filePointer.length());
			final long start = filePointer.getFilePointer();
			serverOutputStream.writeLong(start);
			controller.receivedData(server, start);
			long totalSize = serverInputStream.readLong();
			serverOutputStream.writeLong(totalSize);

			while ((length = serverInputStream.read(b)) != -1) { // TODO
																	// nullPointer...
				forcedAcquire();
				if (server.getSocket() == null)
					return;
				// length is the number of bytes received
				controller.receivedData(server, length);
				aux = Arrays.copyOf(b, length);
				filePointer.write(aux);
				mutex.release();
			}

			if (this.filePointer.length() == totalSize)
				controller.receivingTerminated(true);
			else
				controller.receivingTerminated(false);
		} catch (SocketException e) {
			controller.receivingTerminated(false);
		} catch (IOException e) {
			controller.receivingTerminated(false);
		} catch (NullPointerException e) {
			controller.receivingTerminated(false);
		}
	}

	@Override
	public void run() {
		try {
			server = new User(new Socket(
					FilesSending.options.getSenderAddress(),
					FilesSending.options.getSenderPort()));
			controller.clientConnectedToServer(server);
			download();
		} catch (IOException e) {
			System.out.println("Aucun Serveur à l'adresse "
					+ FilesSending.options.getSenderAddress() + ":"
					+ FilesSending.options.getSenderPort());
			this.controller.serverOffline();
		}
	}

	public void stop() {
		try {
			forcedAcquire();
			if (serverOutputStream != null) {
				serverOutputStream.close();
				serverOutputStream = null;
				System.out.println("Flux de sortie clos.");
			}
			if (serverInputStream != null) {
				serverInputStream.close();
				serverInputStream = null;
				System.out.println("Flux d'entree clos.");
			}
			if (filePointer != null) {
				filePointer.close();
				filePointer = null;
				System.out.println("Flux de sauvegarde clos.");
			}
			if (server != null)
				server.closeSocket();
		} catch (IOException e) {
			e.printStackTrace();
		}
		mutex.release();
	}

	/**
	 * Tells whether the Socket is closed or not.
	 * 
	 * @return Boolean of the state of the Socket.
	 */
	public boolean isSocketOpened() {
		return server.getSocket() != null;
	}
}
