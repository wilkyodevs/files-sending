package gui.controller.main.client;

import gui.controller.main.IndexController;
import gui.controller.main.MainController;
import gui.model.main.FileSizeModel;
import gui.model.main.User;
import gui.model.main.exceptions.UnfindableFileSizeException;
import gui.model.popup.PopupModel;
import gui.model.popup.event.LanguageChangedEvent;
import gui.view.main.EndingView;
import gui.view.main.client.ReceivingClientView;
import gui.view.main.client.ReceivingSettingsView;
import gui.view.main.server.SendingSettingsView;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * The controller for the sending. Manages the connections to the server.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class ReceivingClientController extends MainController {

	/**
	 * The FileSizeModel.
	 */
	private FileSizeModel fsModel;
	/**
	 * The view.
	 */
	private ReceivingClientView view;
	/**
	 * ExecutorService managing the receiving client.
	 */
	private ExecutorService receivingClientExecutor;
	/**
	 * The class to collect the clients.
	 */
	private ReceivingClient client;

	/**
	 * Creates and displays the view.
	 */
	public ReceivingClientController(PopupModel model) {
		super(model);
		fsModel = new FileSizeModel();
		this.view = new ReceivingClientView(this);
		this.fsModel.addFileSizeListener(this.view);
		displayView();
		client = new ReceivingClient(this);
		receivingClientExecutor = Executors.newSingleThreadExecutor();
		receivingClientExecutor.execute(client);
	}

	@Override
	protected void displayView() {
		if (viewDisposed) {
			viewDisposed = false;
			addLanguageListener();
			addPopupWindowStatusListener(this.view);
			this.view.display();
		}
	}

	@Override
	protected void disposeView() {
		if (!viewDisposed) {
			viewDisposed = true;
			removeLanguageListener();
			removePopupWindowStatusListener(this.view);
			this.view.dispose();
		}
	}

	/**
	 * Returns true is the socket is bound to the server.
	 * 
	 * @return True if the client is connected to the server.
	 */
	public boolean isConnectedToServer() {
		if (client == null)
			return false;
		return client.isSocketOpened();
	}

	/**
	 * Called when the ReceivingClient has connected.
	 * 
	 * @param server
	 *            The User linked to the Server.
	 */
	public void clientConnectedToServer(User server) {
		fsModel.addFileSize(server);
		view.refresh();
	}

	/**
	 * Called when the client could not connect to the server.
	 */
	public void serverOffline() {
		receivingClientStopped();
		processNextController(EndingView.CODE_SERVER_OFFLINE);
	}

	/**
	 * Called when the filename has been received.
	 * 
	 * @param filename
	 *            String of the file's name.
	 */
	public void filenameReceived(String filename) {
		System.out.println("Filename is " + filename); // Don't know what to do
	}

	/**
	 * Called when the client has received some data.
	 * 
	 * @param length
	 *            The number of bytes received.
	 */
	public void receivedData(User server, long length) {
		try {
			fsModel.addSize(server, length);
		} catch (UnfindableFileSizeException e) {
			e.printStackTrace();
			receivingClientStopped(); // Useful ?
		}
	}

	/**
	 * Called when the receiving has terminated.
	 */
	public void receivingTerminated(boolean success) {
		System.err.println("receivingTerminated " + success);
		receivingClientStopped();
		if (success) {
			processNextController(EndingView.CODE_FILE_RECEIVED);
		} else {
			processNextController(EndingView.CODE_OTHER_ERROR);
		}
	}

	/**
	 * Called when the Client has stopped downloading.
	 */
	public void receivingClientStopped() {
		client.stop();
		receivingClientExecutor.shutdownNow();
		fsModel.removeAllFileSizes();
		disposeView();
	}

	/**
	 * Closes the client.
	 */
	public void stopReceivingClicked() {
		receivingClientStopped();
		processNextController(EndingView.CODE_CLIENT_DISCONNECTED);
	}

	@Override
	public void sendClicked() {
		receivingClientStopped();
		new IndexController(popupModel, SendingSettingsView.class);
	}

	@Override
	public void receiveClicked() {
		receivingClientStopped();
		new IndexController(popupModel, ReceivingSettingsView.class);
	}

	@Override
	public void languageChanged(LanguageChangedEvent event) {
		view.refresh();
	}
}
