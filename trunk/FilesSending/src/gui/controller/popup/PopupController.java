package gui.controller.popup;

import gui.controller.Controller;
import gui.model.popup.PopupModel;
import application.FilesSending;

/**
 * Superclass for the Popup Controllers.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public abstract class PopupController extends Controller {

	/**
	 * The popup model.
	 */
	protected PopupModel popupModel;

	/**
	 * Sets the model.
	 * 
	 * @param popupModel
	 *            The PopupModel to set
	 */
	public PopupController(PopupModel popupModel) {
		this.popupModel = popupModel;
		this.popupModel.setPopupWindowStatus(true);
	}

	/**
	 * Called when the view has been closed.
	 */
	public void popupWindowClosed() {
		FilesSending.options.refreshLanguage();
		this.disposeView();
		popupModel.setPopupWindowStatus(false);
	}

}
