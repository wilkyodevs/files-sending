package gui.controller.popup;

import gui.model.OptionsModel;
import gui.model.popup.PopupModel;
import gui.view.popup.OptionsView;
import application.FilesSending;
import application.OptionsManager;

/**
 * The controller for the options view.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class OptionsController extends PopupController {

	/**
	 * The view.
	 */
	private OptionsView view;
	/**
	 * The new OptionsModel.
	 */
	private OptionsModel options;

	/**
	 * Creates and displays the view.
	 * 
	 * @param popupModel
	 *            Help Model
	 */
	public OptionsController(PopupModel popupModel) {
		super(popupModel);
		this.options = new OptionsModel(FilesSending.options);
		this.view = new OptionsView(this);
		this.displayView();
	}

	/**
	 * Returns the new OptionsModel.
	 * 
	 * @return the OptionsModel to edit
	 */
	public OptionsModel getOptions() {
		return options;
	}

	@Override
	public void displayView() {
		this.view.display();
	}

	@Override
	public void disposeView() {
		view.dispose();
	}

	/**
	 * Called when the save button has been clicked.
	 */
	public void saveClicked() {
		boolean refreshParent = false;
		if (OptionsManager.save(options)) {
			refreshParent = FilesSending.options.getLanguage() != options
					.getLanguage();
			FilesSending.options = options;
		}
		this.popupWindowClosed();
		if (refreshParent)
			popupModel.fireLanguageChanged(options.getLanguage());
	}

	/**
	 * Called when a language has been chosen.
	 */
	public void notifyLanguageChanged(int language) {
		if (options.getLanguage() != language) {
			options.setLanguage(language);
			view.refresh();
		}
	}
}
