package gui.controller.popup;

import gui.model.popup.PopupModel;
import gui.view.popup.HelpView;

/**
 * The controller for the help view.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class HelpController extends PopupController {

	/**
	 * The view.
	 */
	private HelpView view;

	/**
	 * Creates and displays the view.
	 * 
	 * @param popupModel
	 *            Help Model
	 */
	public HelpController(PopupModel popupModel) {
		super(popupModel);
		this.view = new HelpView(this);
		this.displayView();
	}

	@Override
	public void displayView() {
		this.view.display();
	}

	@Override
	public void disposeView() {
		view.dispose();
	}

	/**
	 * Called when an help label has been clicked.
	 * 
	 * @param index
	 *            Integer of the clicked label.
	 */
	public void labelClicked(int index) {
		view.setHelp(index);
	}
}
