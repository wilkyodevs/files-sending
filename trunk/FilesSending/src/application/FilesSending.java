package application;

import gui.controller.main.IndexController;
import gui.model.OptionsModel;
import gui.model.popup.PopupModel;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * Main Class.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class FilesSending {

	/**
	 * The OptionsModel.
	 */
	public static OptionsModel options;

	/**
	 * Cannot be instantiated.
	 */
	private FilesSending() {
	}

	/**
	 * Launch the choice between sending and receiving a file.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		options = OptionsManager.load();
		new IndexController(new PopupModel());
	}

}
