package application;

import gui.model.OptionsModel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Manages the loading and the saving of the options.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class OptionsManager {

	/**
	 * Default file name.
	 */
	private static final String DEFAULT_FILE_NAME = ".filesSendingOptions";
	/**
	 * Label for the language.
	 */
	private static final String LANGUAGE_LABEL = "LANGUAGE";
	/**
	 * Label for the file to send.
	 */
	private static final String FILE_PATH_LABEL = "FILE";
	/**
	 * Label for the send port.
	 */
	private static final String SEND_PORT_LABEL = "SEND_PORT";
	/**
	 * Label for the size of the packages to send.
	 */
	private static final String PACKAGES_SIZE_LABEL = "PACKAGES_SIZE";
	/**
	 * Label for the server address.
	 */
	private static final String RECEIVE_FROM_LABEL = "RECEIVE_FROM_LABEL";

	/**
	 * Cannot be instantiated.
	 */
	private OptionsManager() {
	}

	/**
	 * Load the default file and returns the options.
	 * 
	 * @return The OptionsModel
	 */
	public static OptionsModel load() {
		return load(DEFAULT_FILE_NAME);
	}

	/**
	 * Load the file given in parameter and returns the options.
	 * 
	 * @param fileName
	 *            File name
	 * @return The OptionsModel
	 */
	public static OptionsModel load(String fileName) {
		//System.out.println("Loading...");
		OptionsModel options = new OptionsModel();
		File f = new File(fileName);
		if (f.exists()) {
			try {
				BufferedReader in = new BufferedReader(new FileReader(f));
				String line;
				while ((line = in.readLine()) != null) {
					String[] split = line.split("=", -1);
					if (split[0].equals(LANGUAGE_LABEL))
						options.setLanguage(Integer.parseInt(split[1]));
					else if (split[0].equals(FILE_PATH_LABEL))
						options.setFilePath(split[1]);
					else if (split[0].equals(SEND_PORT_LABEL))
						options.setSendPort(Integer.parseInt(split[1]));
					else if (split[0].equals(PACKAGES_SIZE_LABEL))
						options.setPackagesSize(Integer.parseInt(split[1]));
					else if (split[0].equals(RECEIVE_FROM_LABEL))
						options.setSenderAddress(split[1]);
				}
				in.close();
			} catch (FileNotFoundException e) {
				System.out
						.println("Problème lors de la lecture du fichier de sauvegarde");
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			save(options);
		}
		LanguageHelper.setLanguage(options.getLanguage());
		return options;
	}

	/**
	 * Saves the options in the default file.
	 * 
	 * @param options
	 *            The OptionsModel to save
	 * @return The success of the save
	 */
	public static boolean save(OptionsModel options) {
		return save(DEFAULT_FILE_NAME, options);
	}

	/**
	 * Saves the options in the file given in parameters.
	 * 
	 * @param fileName
	 *            The name of the file where the options will be saved
	 * @param options
	 *            The OptionsModel to save
	 * @return The success of the save
	 */
	public static boolean save(String fileName, OptionsModel options) {
		//System.out.println("Saving...");
		try {
			File f = new File(fileName);
			PrintWriter out = new PrintWriter(new FileWriter(f));
			try {
				out.println(LANGUAGE_LABEL + "="
						+ String.valueOf(options.getLanguage()));
				out.println(FILE_PATH_LABEL + "=" + options.getFilePath());
				out.println(SEND_PORT_LABEL + "=" + options.getSendPort());
				out.println(PACKAGES_SIZE_LABEL + "="
						+ options.getPackagesSize());
				out.println(RECEIVE_FROM_LABEL + "=" + options.getSenderAddress());
			} finally {
				out.close();
			}
		} catch (IOException e) {
			System.out
					.println("Problème à l'écriture du fichier de sauvegarde");
			return false;
		}
		return true;
	}
}
