package application;

/**
 * Manages the texts in different languages.
 * 
 * @author Willy FRANÇOIS
 * 
 */
public class LanguageHelper {

	/**
	 * The chosen language.
	 */
	private static int language = 0;

	public static final int FILE = 0;
	public static final int SEND = 1;
	public static final int RECEIVE = 2;
	public static final int EXIT = 3;
	public static final int EDIT = 4;
	public static final int OPTIONS = 5;
	public static final int ABOUT = 6;
	public static final int HELP = 7;

	public static final int SEND_RECEIVE_CHOICE = 8;
	public static final int SEND_RECEIVE_CHOICE_TEXT = 9;

	public static final int SENDING_SETTINGS_TEXT = 10;
	public static final int CHOOSE_FILE = 11;
	public static final int CHOOSE_FILE_ERROR = 12;
	public static final int SEND_PORT = 13;
	public static final int SEND_PORT_ERROR = 14;
	public static final int PACKAGES_SIZE = 15;
	public static final int PACKAGES_SIZE_ERROR = 16;
	public static final int BACK = 17;
	public static final int CONTINUE = 18;

	public static final int RECEIVING_SETTINGS_TEXT = 19;
	public static final int SENDER_ADDRESS = 20;
	public static final int SENDER_ADDRESS_ERROR = 21;
	public static final int SENDER_PORT = 22;
	public static final int SENDER_PORT_ERROR = 23;

	public static final int SENDING_SERVER_TEXT = 24;

	public static final int CONNECTING_TO_SERVER_TEXT = 25;
	public static final int RECEIVING_CLIENT_TEXT = 26;

	public static final int END = 27;
	public static final int SERVER_CLOSED_TXT = 28;
	public static final int CLIENT_DISCONNECTED_TXT = 29;
	public static final int FILE_RECEIVED_TXT = 30;
	public static final int SERVER_OFFLINE_TXT = 31;
	public static final int OTHER_ERROR_TXT = 32;

	public static final int ENGLISH = 33;
	public static final int FRENCH = 34;
	public static final int SAVE = 35;
	public static final int CANCEL = 36;

	public static final int CLOSE = 37;

	public static final int HELP_GOAL = 38;
	public static final int HELP_GOAL_TEXT = 39;
	public static final int HELP_OPTIONS = 40;
	public static final int HELP_OPTIONS_TEXT = 41;

	public static final int HELP_BEFORE_SENDING = 42;
	public static final int HELP_BEFORE_SENDING_TEXT = 43;
	public static final int HELP_BEFORE_SENDING_FILE = 44;
	public static final int HELP_BEFORE_SENDING_FILE_TEXT = 45;
	public static final int HELP_BEFORE_SENDING_PORT = 46;
	public static final int HELP_BEFORE_SENDING_PORT_TEXT = 47;
	public static final int HELP_BEFORE_SENDING_PACKAGES = 48;
	public static final int HELP_BEFORE_SENDING_PACKAGES_TEXT = 49;
	public static final int HELP_SENDING = 50;
	public static final int HELP_SENDING_TEXT = 51;

	public static final int HELP_BEFORE_RECEIVING = 52;
	public static final int HELP_BEFORE_RECEIVING_TEXT = 53;
	public static final int HELP_BEFORE_RECEIVING_ADDRESS = 54;
	public static final int HELP_BEFORE_RECEIVING_ADDRESS_TEXT = 55;
	public static final int HELP_BEFORE_RECEIVING_PORT = 56;
	public static final int HELP_BEFORE_RECEIVING_PORT_TEXT = 57;
	public static final int HELP_RECEIVING = 58;
	public static final int HELP_RECEIVING_TEXT = 59;

	/**
	 * Cannot be instantiated.
	 */
	private LanguageHelper() {
	}

	/**
	 * Texts in the different languages.
	 */
	private static final String[][] values = new String[][] {
			{ "File", "Fichier" },
			{ "Send", "Envoyer" },
			{ "Receive", "Recevoir" },
			{ "Exit", "Quitter" },
			{ "Edit", "Éditer" },
			{ "Options", "Options" },
			{ "?", "?" },
			{ "Help", "Aide" },

			{ "Send / Receive Choice", "Choix Envoi / Réception" },
			{ "<html>Please choose what you want to do</html>",
					"<html>Veuillez choisir ce que vous voulez faire</html>" },

			{
					"<html>Please select the file you want to send and from what port you will send it.<br />"
							+ "You can also change the size of the packages that will be sent.</html>",
					"<html>Veuillez sélectionner le fichier à envoyer et le port par lequel il passera.<br />"
							+ "Vous pouvez aussi modifier la taille des paquets envoyés.</html>" },
			{ "Choose a file", "Choisir un fichier" },
			{ "You need to choose an existing file to continue",
					"Veuillez choisir un fichier existant pour continuer" },
			{ "Send Port", "Port d'envoi" },
			{ "The port must be an integer between 1 and 65536",
					"Le port doit être un entier entre 1 et 65536" },
			{ "Packages size", "Taille des paquets" },
			{ "The packages's size must be an integer",
					"La taille des paquets doit être un entier" },
			{ "Back", "Retour" },
			{ "Continue", "Continuer" },

			{
					"<html>Please enter the address and the port from where you would like to receive the file.</html>",
					"<html>Veuillez entrer l'adresse et le port d'où télécharger le fichier.</html>" },
			{ "Sender address", "Adresse de l'envoyeur" },
			{ "You need to enter the address of the sender",
					"Veuillez entrer l'adresse de l'envoyeur" },
			{ "Sender port", "Port de l'envoyeur" },
			{ "You need to enter the port of the sender",
					"Veuillez entrer le port de l'envoyeur" },

			{ "<html>Waiting for clients...</html>",
					"<html>En attente de clients...</html>" },

			{ "<html>Connecting to sender...</html>",
					"<html>Connexion à l'envoyeur...</html>" },

			{ "<html>Currently receiving...</html>",
					"<html>Réception en cours...</html>" },

			{ "End", "Fin" },
			{ "The Server is closed", "Le Serveur est fermé" },
			{ "The Client is disconnected", "Le Client est déconnecté" },
			{ "The file has been successfully downloaded",
					"Le fichier a été téléchargé avec succès" },
			{ "The server is currently offline",
					"Le serveur est actuellement hors-ligne" },
			{ "An error occured", "Une erreur est survenue" },

			{ "English", "Anglais" },
			{ "French", "Français" },
			{ "Save", "Enregistrer" },
			{ "Cancel", "Annuler" },
			{ "Close", "Fermer" },

			{ "Goal", "Intérêt" },
			{ "<html>This program lets you send files by the network.</html>",
					"<html>Ce programme permet d'envoyer des fichiers via le réseau.</html>" },
			{ "Options", "Options" },
			{
					"<html>You can edit the options via the Edit Menu.<br /><br />"
							+ "The only available option is the language you want.</html>",
					"<html>Vous pouvez modifier les options via le menu Éditer.<br /><br />"
							+ "La seule option disponible est le choix de la langue.</html>" },
			{ "Before the Sending", "Avant d'envoyer" },
			{
					"<html>Before the sending, you need to choose the file to send, the port where the connections will be done and the packages length.</html>",
					"<html>Avant d'envoyer, il faut choisir le fichier à envoyer, le port sur lequel on va ouvrir les connexions et la taille des paquets.</html>" },
			{ "    File", "    Fichier" },
			{
					"<html>You can choose any type of file from the File Chooser by clicking the corresponding button.</html>",
					"<html>Vous pouvez choisir n'importe quel type de fichier en cliquant sur le bouton correspondant.</html>" },
			{ "    Port", "    Port" },
			{
					"<html>It determines which door is opened on your computer to send the file.<br /><br />"
							+ "If you would like to send multiple files, you'll need to open a new instance of the Program and specify another port.</html>",
					"<html>Il définit quelle porte est ouverte sur votre machine afin d'envoyer le fichier.<br /><br />"
							+ "Si vous voulez envoyer plusieurs fichiers, il faudra ouvrir une nouvelle instance du programme et y spécifier un autre port.</html>" },
			{ "    Packages' size", "    Taille des paquets" },
			{
					"<html>It Determines the size of the packages you will send via the network.<br /><br />"
							+ "If they are too large, it could take more time to scatter them.<br /><br />"
							+ "By the way, your network card scatters them again.</html>",
					"<html>Elle définit la taille des paquets qui vont transiter via le réseau.<br /><br />"
							+ "S'ils sont trop gros, cela prendra du temps de les découper.<br /><br />"
							+ "Cependant, la carte réseau découpera les paquets afin qu'ils puissent circuler sans problèmes.</html>" },
			{ "Sending", "Envoi" },
			{
					"<html>As soon as you validated your parameters, the sending can begin.<br /><br />"
							+ "The program will listen for clients to connect.<br /><br />"
							+ "When a client is connected, the file begins being sent to it until it has finished or an error occurs.</html>",
					"<html>Dès que vous validez vos paramètres, l'envoi peut commencer.<br /><br />"
							+ "Le programme se met alors à l'écoute de clients souhaitant se connecter.<br /><br />"
							+ "Quand un client est connecté, le fichier commence à être envoyé jusqu'à la fin ou jusqu'à ce qu'une erreur se produise.</html>" },

			{ "Before Receiving", "Avant réception" },
			{
					"<html>Before receiving, the ip address and the port must be specified.</html>",
					"<html>Avant de recevoir, il faut définir l'adresse ip du serveur et le port par lequel passer.</html>" },
			{ "    Server Address", "    Adresse du Serveur" },
			{
					"<html>It's the ip address of the computer which launched the program in sending mode.</html>",
					"<html>C'est l'adresse ip de la machine qui a lancé le programme en mode envoi.</html>" },
			{ "    Server Port", "    Port du Serveur" },
			{
					"<html>It determines which door is opened on the server computer to receive the file.<br /><br />"
							+ "If you would like to receive multiple files, you'll need to open a new instance of the Program and specify another port.</html>",
					"<html>Il définit quelle porte est ouverte sur la machine serveur afin de recevoir le fichier.<br /><br />"
							+ "Si vous voulez recevoir plusieurs fichiers, il faudra ouvrir une nouvelle instance du programme et y spécifier un autre port.</html>" },
			{ "Receiving", "Réception" },
			{
					"<html>As soon as you validated your parameters, the receiving can begin.<br /><br />"
							+ "The program will try to connect to the server.<br /><br />"
							+ "When the connection succeeded, the file begins being received from it until it has finished or an error occurs.</html>",
					"<html>Dès que vous validez vos paramètres, la réception peut commencer.<br /><br />"
							+ "Le programme essaye alors de se connecter au serveur.<br /><br />"
							+ "Quand la connexion est établie, le fichier commence à être reçu jusqu'à la fin ou jusqu'à ce qu'une erreur se produise.</html>" } };

	/**
	 * Sets the chosen language.
	 * 
	 * @param language
	 *            Index of the chosen language.
	 */
	public static void setLanguage(int language) {
		LanguageHelper.language = language;
	}

	/**
	 * Retrieve the wanted text in the chosen language.
	 * 
	 * @param text
	 *            Index of the text to get.
	 * @return The correct translation of the wanted text.
	 */
	public static String getText(int text) {
		return values[text][language];
	}
}
